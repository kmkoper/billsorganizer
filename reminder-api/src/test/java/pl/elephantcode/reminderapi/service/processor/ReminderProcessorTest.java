package pl.elephantcode.reminderapi.service.processor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.reminderapi.model.Invoice;
import pl.elephantcode.reminderapi.model.Reminder;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ReminderProcessorTest {

    ReminderProcessor processor = new ReminderProcessor();

    Invoice invoice;

    LocalDateTime paymentDate = LocalDateTime.now().plusMonths(1);
    Integer reminderBeforeValue = 2;

    @BeforeEach
    void setUp() {
        invoice = new Invoice();
        invoice.setActive(false);
        invoice.setPaymentDate(paymentDate);
        invoice.setReminderBeforePaymentValue(reminderBeforeValue);
        invoice.setReminderBeforePaymentType("day");

    }

    @Test
    void processWhenInvoiceIsInactiveShouldReminderDateBeNull() {
        //given
        invoice.setActive(false);

        //when
        Reminder reminder = processor.process(invoice);

        //then
        assertEquals(invoice.getPaymentDate(), reminder.getPaymentDate());
        assertNull(reminder.getReminderDate(), "Reminder date should be null");
        assertNull(reminder.getLastReminderDate(), "Last reminder date should be null");

    }

    @Test
    void processWhenInvoiceIsActive() {
        //given
        invoice.setActive(true);

        //when
        Reminder reminder = processor.process(invoice);

        //then
        assertNotNull(reminder.getReminderDate());
        assertNotNull(reminder.getLastReminderDate());
        assertEquals(paymentDate.minusDays(reminderBeforeValue), reminder.getReminderDate());
        assertEquals(paymentDate.minusDays(1), reminder.getLastReminderDate());
    }

    @Test
    void processWhenPeriodTypeIsNotRecognised(){
        //given
        invoice.setActive(true);
        invoice.setReminderBeforePaymentType("not valid");

        //then
        assertThrows(IllegalArgumentException.class, () ->
            processor.process(invoice));
    }
}