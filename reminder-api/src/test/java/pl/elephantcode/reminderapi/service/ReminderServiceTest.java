package pl.elephantcode.reminderapi.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.reminderapi.model.Invoice;
import pl.elephantcode.reminderapi.model.Reminder;
import pl.elephantcode.reminderapi.repository.ReminderRepository;
import pl.elephantcode.reminderapi.service.processor.ReminderProcessor;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ReminderServiceTest {

    @Mock
    ReminderProcessor reminderProcessor;

    @Mock
    ReminderRepository reminderRepository;

    @InjectMocks
    ReminderService reminderService;

    Invoice invoice;
    Reminder oldReminder;
    Reminder reminder;
    String ID1 = "id1";
    LocalDateTime beforeUpdate = LocalDateTime.now().minusDays(1);
    LocalDateTime afterUpdate = LocalDateTime.now().plusDays(1);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        invoice = new Invoice();
        invoice.setId(1);
        invoice.setCurrentPaid(true);
        invoice.setReminderBeforePaymentType("day");
        invoice.setPaymentDate(LocalDateTime.now());
        invoice.setReminderBeforePaymentValue(2);
        invoice.setPaymentDate(afterUpdate);
        invoice.setActive(true);

        oldReminder = Reminder.builder()
                .id(ID1).paid(false).invoiceId(1).reminderSent(true).paymentDate(beforeUpdate).build();

        reminder = Reminder.builder()
                .id(ID1).invoiceId(1).reminderSent(false).paymentDate(afterUpdate).build();
    }

    @Test
    void update() {
        when(reminderProcessor.process(any(Invoice.class))).thenCallRealMethod();
        when(reminderRepository.findByInvoiceId(anyInt())).thenReturn(Optional.of(oldReminder));

        Reminder updatedReminder = reminderService.updateReminder(invoice);

        assertTrue(updatedReminder.isPaid());
        assertTrue(updatedReminder.isReminderSent());
        assertEquals(invoice.getId(), updatedReminder.getInvoiceId());
        assertEquals(oldReminder.getId(), updatedReminder.getId());
        assertEquals(invoice.getPaymentDate(), updatedReminder.getPaymentDate());
    }

    @Test
    void updateWhenOldNotfound() {
        //given
        when(reminderProcessor.process(any(Invoice.class))).thenCallRealMethod();
        when(reminderRepository.findByInvoiceId(anyInt())).thenReturn(Optional.empty());

        Reminder processedReminder = reminderProcessor.process(invoice);
        when(reminderRepository.save(any(Reminder.class))).thenReturn(processedReminder);

        //when
        Reminder reminder = reminderService.updateReminder(invoice);

        //then
        assertEquals(invoice.getId(), reminder.getInvoiceId());
        assertEquals(invoice.getPaymentDate(), reminder.getPaymentDate());
        assertEquals(invoice.isCurrentPaid(), reminder.isPaid());

        verify(reminderRepository, times(1)).save(any(Reminder.class));
        verify(reminderRepository, times(1)).findByInvoiceId(anyInt());
    }

    @Test
    void create() {
        when(reminderProcessor.process(any(Invoice.class))).thenCallRealMethod();
        when(reminderRepository.save(any(Reminder.class))).thenReturn(reminder);

        Reminder createdReminder = reminderService.createReminder(invoice);

        assertFalse(createdReminder.isPaid(), "Should be the same as in invoice");
        assertFalse(createdReminder.isReminderSent());
        assertEquals(invoice.getId(), createdReminder.getInvoiceId(), "Should be the same");
        assertEquals(invoice.getPaymentDate(), createdReminder.getPaymentDate(), "Should be the same as in invoice");
    }
}