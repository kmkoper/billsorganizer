package pl.elephantcode.reminderapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.elephantcode.reminderapi.model.Reminder;
import pl.elephantcode.reminderapi.model.ReminderList;
import pl.elephantcode.reminderapi.repository.ReminderRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ReminderResourceTest {

    @InjectMocks
    ReminderResource reminderResource;

    @Mock
    ReminderRepository repository;

    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(reminderResource).build();
    }

    @Test
    void findAllReminders() throws Exception {
        Reminder reminder = new Reminder();
        reminder.setInvoiceId(1);

        Reminder reminder1 = new Reminder();
        reminder1.setInvoiceId(2);

        List<Reminder> reminders = new ArrayList<>();

        when(reminderResource.getAllReminders()).thenReturn(reminders);
        mockMvc.perform(get(ReminderResource.BASE_URL + "/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(repository, times(1)).findAll();
    }

    @Test
    void findReminder() throws Exception {
        Reminder reminder = new Reminder();
        reminder.setInvoiceId(1);
        reminder.setId("5c36901e1e414f3e28e4b4d0");

        when(reminderResource.getReminderById(anyString())).thenReturn(Optional.of(reminder));
        mockMvc.perform(get(ReminderResource.BASE_URL + "/5c36901e1e414f3e28e4b4d0")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo("5c36901e1e414f3e28e4b4d0")))
                .andExpect(jsonPath("$.invoiceId", not("")));
        verify(repository, times(1)).findById(anyString());
    }

    @Test
    void createReminder() throws Exception {
        Reminder reminder = new Reminder();
        reminder.setInvoiceId(1);
        reminder.setId("5c36901e1e414f3e28e4b4d0");

        when(repository.save(any(Reminder.class))).thenReturn(reminder);

        mockMvc.perform(post(ReminderResource.BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reminder)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.invoiceId", equalTo(1)));

        verify(repository, times(1)).save(any(Reminder.class));
    }

    @Test
    void updateReminder() throws Exception {
        Reminder reminder = new Reminder();
        reminder.setInvoiceId(1);
        reminder.setId("5c36901e1e414f3e28e4b4d0");

        when(repository.findById(any(String.class))).thenReturn(Optional.of(reminder));
        when(repository.save(any(Reminder.class))).thenReturn(reminder);
        mockMvc.perform(put(ReminderResource.BASE_URL + "/5c36901e1e414f3e28e4b4d0")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reminder)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.invoiceId", equalTo(1)))
                .andExpect(jsonPath("$.id", equalTo("5c36901e1e414f3e28e4b4d0")));

        verify(repository, times(1)).save(any(Reminder.class));
    }

    @Test
    void deleteReminder() {
        Reminder reminder = new Reminder();
        reminder.setInvoiceId(1);
        reminder.setId("5c36901e1e414f3e28e4b4d0");

        doNothing().when(repository).deleteById(reminder.getId());
        when(repository.existsById(anyString())).thenReturn(true);

        reminderResource.deleteReminder(anyString());

        verify(repository, times(1)).deleteById(any(String.class));
        verify(repository, times(1)).existsById(any(String.class));


    }

    @Test
    void getForSendReminder() {
        Reminder reminder = new Reminder();
        List<Reminder> reminders = new ArrayList<>();
        reminders.add(reminder);

        when(repository.findAllForSendingReminder(any(LocalDateTime.class))).thenReturn(reminders);

        ReminderList reminderList = reminderResource.getForSendReminder();

        verify(repository, times(1)).findAllForSendingReminder(any(LocalDateTime.class));

        Assertions.assertTrue(reminderList.getReminders().contains(reminder), "Should contain simple Reminder");
    }

    @Test
    void getForSendLastReminder() {
        Reminder reminder = new Reminder();
        List<Reminder> reminders = new ArrayList<>();
        reminders.add(reminder);

        when(repository.findAllForSendingLastReminder(any(LocalDateTime.class))).thenReturn(reminders);

        ReminderList reminderList = reminderResource.getForSendLastReminder();

        verify(repository, times(1)).findAllForSendingLastReminder(any(LocalDateTime.class));
        Assertions.assertTrue(reminderList.getReminders().contains(reminder), "Should contain simple Reminder");
    }

    @Test
    void markReminderSentTrueById() {
        Reminder reminder = new Reminder();
        reminder.setReminderSent(false);

        when(repository.findById(anyString())).thenReturn(Optional.of(reminder));

        reminderResource.markReminderSentTrueById(anyString());

        verify(repository, times(1)).save(any(Reminder.class));
        verify(repository, times(1)).findById(anyString());
        Assertions.assertTrue(reminder.isReminderSent());
    }

    @Test
    void markLastReminderSentTrueById() {
        Reminder reminder = new Reminder();
        reminder.setLastReminderSent(false);

        when(repository.findById(anyString())).thenReturn(Optional.of(reminder));

        reminderResource.markLastReminderSentTrueById(anyString());

        verify(repository, times(1)).save(any(Reminder.class));
        verify(repository, times(1)).findById(anyString());
        Assertions.assertTrue(reminder.isLastReminderSent());
    }
}
