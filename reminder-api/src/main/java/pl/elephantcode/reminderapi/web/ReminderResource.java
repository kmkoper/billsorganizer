package pl.elephantcode.reminderapi.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.reminderapi.model.Reminder;
import pl.elephantcode.reminderapi.model.ReminderList;
import pl.elephantcode.reminderapi.repository.ReminderRepository;
import pl.elephantcode.reminderapi.web.error.ResourceNotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static pl.elephantcode.reminderapi.web.ReminderResource.BASE_URL;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class ReminderResource {

    public static final String BASE_URL = "/api/reminders";

    private final ReminderRepository reminderRepository;

    @GetMapping(value = "/")
    public List<Reminder> getAllReminders() {
        log.info("Fetching all reminders");
        return reminderRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Reminder createReminder(@RequestBody Reminder reminder) {
        reminderRepository.save(reminder);
        log.info("Saving reminder id: {" + reminder.getId() + "}");
        return reminder;

    }

    @GetMapping(value = "/{reminderId}")
    public Optional<Reminder> getReminderById(@PathVariable("reminderId") String reminderId) {
        log.info("Fething reminder id: {" + reminderId + "}");
        return reminderRepository.findById(reminderId);
    }

    @PutMapping(value = "/{reminderId}")
    @ResponseStatus(HttpStatus.OK)
    public Reminder updateReminder(
            @PathVariable("reminderId") String reminderId,
            @RequestBody Reminder reminder) {
        final Optional<Reminder> foundReminder = reminderRepository.findById(reminderId);
        if (foundReminder.isPresent()) {
            log.info("Updating reminder id: {" + reminderId + "}");
            reminder.setId(reminderId);
            reminder.setCreatedAt(foundReminder.get().getCreatedAt()); //custom handling for @CreatedDate
            return reminderRepository.save(reminder);
        } else {
            throw new ResourceNotFoundException("Cannot update reminder id: {" + reminderId + "} Reminder not found!");
        }
    }

    @DeleteMapping(value = "/{reminderId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteReminder(@PathVariable("reminderId") String reminderId) {
        if (reminderRepository.existsById(reminderId)) {
            reminderRepository.deleteById(reminderId);
            log.info("Reminder id: {" + reminderId + "} deleted");
        } else {
            throw new ResourceNotFoundException("Cannot delete reminder id: {" + reminderId + "} Reminder not found!");
        }
    }

    @GetMapping(value = "/send-reminder")
    public ReminderList getForSendReminder(){
        ReminderList reminderList = new ReminderList();
        reminderList.setReminders(reminderRepository.findAllForSendingReminder(LocalDateTime.now()));
        log.info("Fetching for reminders to send");
        return reminderList;
    }

    @GetMapping(value = "/send-last-reminder")
    public ReminderList getForSendLastReminder(){
        ReminderList reminderList = new ReminderList();
        reminderList.setReminders(reminderRepository.findAllForSendingLastReminder(LocalDateTime.now()));
        log.info("Fetching for last reminders to send");
        return reminderList;
    }

    @PostMapping(value = "/send-reminder/disable/{reminderId}")
    public Reminder markReminderSentTrueById(
            @PathVariable("reminderId") String reminderId){
        final Optional<Reminder> foundReminder = reminderRepository.findById(reminderId);

        Reminder reminder = foundReminder.orElseThrow( () ->
                new ResourceNotFoundException(
                        "Cannot set reminder_sent = true, reminder {" + reminderId + "} does not exist!"));

        reminder.setReminderSent(true);
        log.info("Reminder id {" + reminderId + "} marked as sent!");
        return reminderRepository.save(reminder);
    }

    @PostMapping(value = "/send-last-reminder/disable/{reminderId}")
    public Reminder markLastReminderSentTrueById(
            @PathVariable("reminderId") String reminderId){
        final Optional<Reminder> foundReminder = reminderRepository.findById(reminderId);

        Reminder reminder = foundReminder.orElseThrow( () ->
                new ResourceNotFoundException(
                        "Cannot set reminder_sent = true, reminder {" + reminderId + "} does not exist!"));

        reminder.setLastReminderSent(true);
        log.info("Reminder id {" + reminderId + "} marked as sent!");
        return reminderRepository.save(reminder);
    }
}
