package pl.elephantcode.reminderapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import pl.elephantcode.reminderapi.model.Reminder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReminderRepository extends MongoRepository<Reminder, String> {

    @Query(value = "{ 'paid' : false, 'reminderDate' : {$lte: ?0 }, 'reminderSent' : false }")
    List<Reminder> findAllForSendingReminder(LocalDateTime currentTimestamp);

    @Query(value = "{ 'paid' : false, 'lastReminderDate' : {$lte: ?0 }, 'lastReminderSent' : false }")
    List<Reminder> findAllForSendingLastReminder(LocalDateTime currentTimestamp);

    Optional<Reminder> findByInvoiceId(Integer invoiceId);
}
