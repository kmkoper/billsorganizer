package pl.elephantcode.reminderapi.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class Invoice {

    private Integer id;
    private String description;
    private Float amount;
    private Integer userId;
    private boolean isActive;
    private boolean isRecurring;
    private Integer paymentPeriod;
    private String paymentPeriodType;
    private boolean isCurrentPaid;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime paymentDate;
    private Integer reminderBeforePaymentValue;
    private String reminderBeforePaymentType;
}
