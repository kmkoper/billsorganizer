package pl.elephantcode.reminderapi.model;

import lombok.Data;

import java.util.List;

@Data
public class ReminderList {

    private List<Reminder> reminders;
}
