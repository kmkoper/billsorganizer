package pl.elephantcode.reminderapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document(value = "reminders")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Reminder {

    @Id
    private String id;

    @Field(value = "invoice_id")
    private Integer invoiceId;
    @Field(value = "user_id")
    private Integer userId;
    private LocalDateTime reminderDate;
    private boolean reminderSent;
    private LocalDateTime lastReminderDate;
    private boolean lastReminderSent;
    private LocalDateTime paymentDate;
    private boolean paid;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;
}
