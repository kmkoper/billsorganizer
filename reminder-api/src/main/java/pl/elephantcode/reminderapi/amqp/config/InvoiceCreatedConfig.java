package pl.elephantcode.reminderapi.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class InvoiceCreatedConfig {
    public static final String QUEUE_NAME = "reminder-api-invoice-created";

    @Value("${rabbitmq.routing-key.invoice-created}")
    private String ROUTING_KEY;

    @Bean
    Queue invoiceCreatedQueue() {
        return new Queue(QUEUE_NAME, false);
    }

    @Bean
    Binding invoiceCreatedBinding(Queue invoiceCreatedQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(invoiceCreatedQueue).to(topicExchange).with(ROUTING_KEY);
    }
}
