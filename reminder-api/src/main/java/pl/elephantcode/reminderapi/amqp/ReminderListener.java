package pl.elephantcode.reminderapi.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import pl.elephantcode.reminderapi.amqp.config.InvoiceCreatedConfig;
import pl.elephantcode.reminderapi.amqp.config.InvoiceModifiedConfig;
import pl.elephantcode.reminderapi.model.Invoice;
import pl.elephantcode.reminderapi.service.ReminderService;

import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReminderListener {

    private final ReminderService reminderService;
    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues = InvoiceModifiedConfig.QUEUE_NAME)
    public void invoiceModifiedListener(String message) {
        log.debug("Invoice modified message received from queue");

        try {
            reminderService.updateReminder(invoiceReader(message));

        } catch (Exception e) {
            log.error("Exception occurs while processing message from queue: {} " + e, InvoiceModifiedConfig.QUEUE_NAME);
            throw new AmqpRejectAndDontRequeueException(e.getMessage());
        }
    }
    @RabbitListener(queues = InvoiceCreatedConfig.QUEUE_NAME)
    public void invoiceCreatedListener(String message){
        log.debug("Invoice created message received from queue");

        try {
            reminderService.createReminder(invoiceReader(message));

        } catch (Exception e) {
            log.error("Exception occurs while processing message from queue: {} " + e, InvoiceModifiedConfig.QUEUE_NAME);
            throw new AmqpRejectAndDontRequeueException(e.getMessage());
        }
    }

    private Invoice invoiceReader(String message) throws IOException {
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.readValue(message, Invoice.class);
    }


}
