package pl.elephantcode.reminderapi.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class InvoiceModifiedConfig {

    public static final String QUEUE_NAME = "reminder-api-invoice-modified";

    @Value("${rabbitmq.routing-key.invoice-modified}")
    private String ROUTING_KEY;

    @Bean
    Queue invoiceModifiedQueue() {
        return new Queue(QUEUE_NAME, false);
    }

    @Bean
    Binding invoiceModifiedBinding(Queue invoiceModifiedQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(invoiceModifiedQueue).to(topicExchange).with(ROUTING_KEY);
    }
}
