package pl.elephantcode.reminderapi.service.processor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.reminderapi.model.Invoice;
import pl.elephantcode.reminderapi.model.Reminder;

import java.time.LocalDateTime;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReminderProcessor {

    public Reminder process(Invoice invoice) {
        LocalDateTime reminderDate;
        Integer interval = invoice.getReminderBeforePaymentValue();
        LocalDateTime paymentDate = invoice.getPaymentDate();
        if(invoice.isActive()){
        switch (invoice.getReminderBeforePaymentType()) {
            case "day":
                reminderDate = paymentDate.minusDays(interval);
                break;
            case "month":
                reminderDate = paymentDate.minusMonths(interval);
                break;
            case "year":
                reminderDate = paymentDate.minusYears(interval);
                break;
            default:
                throw new IllegalArgumentException("Reminder period type {" + invoice.getReminderBeforePaymentType() +
                        "} not recognised! Cannot create reminder for invoice id: {" + invoice.getId() + "}");
                }
        } else {
            reminderDate = null;
        }
        return Reminder.builder()
                .invoiceId(invoice.getId())
                .userId(invoice.getUserId())
                .reminderDate(reminderDate)
                .reminderSent(false)
                .lastReminderDate(calculateLastReminderDate(invoice))
                .lastReminderSent(false)
                .paymentDate(invoice.getPaymentDate())
                .paid(invoice.isCurrentPaid())
                .build();
    }
    private LocalDateTime calculateLastReminderDate(Invoice invoice){
        if(invoice.isActive()) {
            return invoice.getPaymentDate().minusDays(1);
        } else {
            return null;
        }
    }
}
