package pl.elephantcode.reminderapi.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.reminderapi.model.Invoice;
import pl.elephantcode.reminderapi.model.Reminder;
import pl.elephantcode.reminderapi.repository.ReminderRepository;
import pl.elephantcode.reminderapi.service.processor.ReminderProcessor;

import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReminderService {
    private final ReminderProcessor processor;
    private final ReminderRepository reminderRepository;

    public Reminder updateReminder(Invoice invoice) {
        Optional<Reminder> reminderToUpdate = reminderRepository.findByInvoiceId(invoice.getId());

        if (reminderToUpdate.isPresent()) {
            Reminder updatedReminder = processor.process(invoice);
            mapOldReminderToNew(updatedReminder, reminderToUpdate.get());
            reminderRepository.save(updatedReminder);
            log.info("Reminder {} for invoice {} successfully updated", updatedReminder.getId(), invoice.getId());
            return updatedReminder;
        } else {
            log.error("Reminder for invoice id: {} not found! Creating new", invoice.getId());
            return createReminder(invoice);
        }
    }

    public Reminder createReminder(Invoice invoice) {
        log.info("Creating reminder for invoice: {}", invoice.getId());
        Reminder reminder = processor.process(invoice);
        return reminderRepository.save(reminder);
    }

    private void mapOldReminderToNew(Reminder updatedReminder, Reminder toUpdate) {
        updatedReminder.setId(toUpdate.getId());
        updatedReminder.setReminderSent(toUpdate.isReminderSent());
        updatedReminder.setLastReminderSent(toUpdate.isLastReminderSent());
    }
}
