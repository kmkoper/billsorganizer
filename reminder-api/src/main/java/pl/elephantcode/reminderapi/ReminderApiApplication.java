package pl.elephantcode.reminderapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@Slf4j
@EnableEurekaClient
@SpringBootApplication
@EnableMongoAuditing
public class ReminderApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReminderApiApplication.class, args);
    }

}

