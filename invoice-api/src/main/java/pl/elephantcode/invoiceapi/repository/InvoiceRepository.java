package pl.elephantcode.invoiceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.elephantcode.invoiceapi.model.Invoice;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

    @Query("SELECT i FROM Invoice i WHERE i.userId = ?1")
    List<Invoice> findAllByUserId(Integer userId);

    @Query("SELECT i FROM Invoice i " +
            "WHERE i.isActive=true " +
            "and isRecurring=true " +
            "and i.paymentDate < CURRENT_DATE")
    List<Invoice> findForCreatingNextInvoice();
}
