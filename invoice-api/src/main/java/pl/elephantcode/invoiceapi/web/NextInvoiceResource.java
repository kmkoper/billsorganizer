package pl.elephantcode.invoiceapi.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.invoiceapi.mapper.InvoiceConverter;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;
import pl.elephantcode.invoiceapi.repository.InvoiceRepository;

import java.util.Optional;

import static pl.elephantcode.invoiceapi.web.NextInvoiceResource.BASE_URL;

@RequestMapping(BASE_URL)
@RestController
@RequiredArgsConstructor
@Slf4j
public class NextInvoiceResource {
    public static final String BASE_URL = "/api/invoices/create-next";

    private final InvoiceRepository invoiceRepository;
    private final InvoiceConverter invoiceConverter;

    @GetMapping()
    public InvoiceDTOList getDataForNextInvoice() {
        log.info("Fetching data for creating next invoices");
        return invoiceConverter.listInvoiceToInvoiceDTOListObj(
                invoiceRepository.findForCreatingNextInvoice());
    }

    @PostMapping(value = "/disable/{invoiceId}")
    @ResponseStatus(HttpStatus.OK)
    public void inactivateInvoice(@PathVariable("invoiceId") Integer invoiceId) {

        final Optional<Invoice> foundInvoice = invoiceRepository.findById(invoiceId);

        Invoice invoice = foundInvoice.orElseThrow(() ->
                new ResourceNotFoundException(
                        "Cannot inactivate invoice id: {" + invoiceId + "} Invoice not found!"));
        invoice.setActive(false);
        invoiceRepository.save(invoice);
        log.info("Invoice id: {" + invoiceId + "} inactivated");
    }
}
