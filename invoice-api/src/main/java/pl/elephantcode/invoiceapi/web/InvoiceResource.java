package pl.elephantcode.invoiceapi.web;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.invoiceapi.amqp.InvoicePublisher;
import pl.elephantcode.invoiceapi.mapper.InvoiceConverter;
import pl.elephantcode.invoiceapi.mapper.InvoiceMapper;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTO;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;
import pl.elephantcode.invoiceapi.repository.InvoiceRepository;

import java.util.Optional;

import static pl.elephantcode.invoiceapi.web.InvoiceResource.BASE_URL;

@RequestMapping(BASE_URL)
@RestController
@RequiredArgsConstructor
@Slf4j
public class InvoiceResource {

    public static final String BASE_URL = "/api/invoices";

    private final InvoiceRepository invoiceRepository;
    private final InvoiceMapper invoiceMapper;
    private final InvoiceConverter invoiceConverter;
    private final InvoicePublisher invoicePublisher;

    @Value("${rabbitmq.routing-key.invoice-modified}")
    private String INVOICE_MODIFIED;

    @Value("${rabbitmq.routing-key.invoice-created}")
    private String INVOICE_CREATED;

    @GetMapping(value = "/{invoiceId}")
    public Optional<Invoice> findInvoice(@PathVariable("invoiceId") Integer invoiceId) {
        log.info("Fetching invoice id: {}", invoiceId);
        return invoiceRepository.findById(invoiceId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Invoice createInvoice(@RequestBody InvoiceDTO invoiceDTO) {
        Invoice invoice = invoiceMapper.invoiceDTOToInvoice(invoiceDTO);
        invoice.setId(null);
        invoiceRepository.save(invoice);
        log.info("Saving invoice id: " + invoice.getId());
        invoicePublisher.publishMessage(invoice, INVOICE_CREATED);
        return invoice;
    }

    @PutMapping(value = "/{invoiceId}")
    @ResponseStatus(HttpStatus.OK)
    public Invoice updateInvoice(
            @PathVariable("invoiceId") Integer invoiceId,
            @RequestBody InvoiceDTO invoiceDTO) {

        if(invoiceRepository.existsById(invoiceId)){
            Invoice invoice = invoiceMapper.invoiceDTOToInvoice(invoiceDTO);
            invoice.setId(invoiceId);

            log.info("Updating invoice id: {}", invoiceId);
            invoiceRepository.save(invoice);
            invoicePublisher.publishMessage(invoice, INVOICE_MODIFIED);
            return invoice;

        } else {
            throw new ResourceNotFoundException("Cannot update invoice id: {" + invoiceId + " Invoice not found!");
        }
    }

    @DeleteMapping(value = "/{invoiceId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteInvoiceById(@PathVariable("invoiceId") Integer invoiceId) {
        if (invoiceRepository.existsById(invoiceId)) {
            invoiceRepository.deleteById(invoiceId);
            log.info("Invoice id: {} deleted", invoiceId);
        } else {
            throw new ResourceNotFoundException("Given invoice id {" + invoiceId + "} does not exist!");
        }
    }

    @GetMapping(value = "/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public InvoiceDTOList findAllInvoicesOfUserId(@PathVariable("userId") Integer userId) {
        log.info("Fetching invoices of user id {}", userId);
        InvoiceDTOList invoices = new InvoiceDTOList();
        for (Invoice invoice : invoiceRepository.findAllByUserId(userId)) {
            invoices.getInvoiceDTOList().add(invoiceMapper.invoiceToInvoiceDTO(invoice));
        }
        invoiceConverter.listInvoiceToInvoiceDTOListObj(invoiceRepository.findAllByUserId(userId));
        return invoices;
    }
}
