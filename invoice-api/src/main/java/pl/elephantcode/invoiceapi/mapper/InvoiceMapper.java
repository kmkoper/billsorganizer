package pl.elephantcode.invoiceapi.mapper;

import org.mapstruct.Mapper;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTO;
import pl.elephantcode.invoiceapi.model.Invoice;

@Mapper(componentModel = "spring")
public interface InvoiceMapper {

    InvoiceDTO invoiceToInvoiceDTO(Invoice invoice);

    Invoice invoiceDTOToInvoice(InvoiceDTO invoiceDTO);
}
