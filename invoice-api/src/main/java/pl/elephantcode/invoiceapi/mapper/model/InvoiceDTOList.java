package pl.elephantcode.invoiceapi.mapper.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InvoiceDTOList {
    private List<InvoiceDTO> invoiceDTOList;

    public InvoiceDTOList() {
        invoiceDTOList = new ArrayList<>();
    }
}
