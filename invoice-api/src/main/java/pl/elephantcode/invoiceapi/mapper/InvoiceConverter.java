package pl.elephantcode.invoiceapi.mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class InvoiceConverter {

    private final InvoiceMapper invoiceMapper;

    public InvoiceDTOList listInvoiceToInvoiceDTOListObj(List<Invoice> invoices) {
        InvoiceDTOList invoiceDTOList = new InvoiceDTOList();
        for (Invoice inv : invoices) {
            invoiceDTOList.getInvoiceDTOList().add(invoiceMapper.invoiceToInvoiceDTO(inv));
        }
        return invoiceDTOList;
    }
}
