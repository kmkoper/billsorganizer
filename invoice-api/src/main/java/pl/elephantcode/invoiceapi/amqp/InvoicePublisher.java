package pl.elephantcode.invoiceapi.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.elephantcode.invoiceapi.model.Invoice;

@Slf4j
@Component
@RequiredArgsConstructor
public class InvoicePublisher {
    private final RabbitTemplate rabbitTemplate;
    @Value("${rabbitmq.exchange-name-topic}")
    private String EXCHANGE_NAME;
    private ObjectMapper objectMapper = new ObjectMapper();

    public void publishMessage(Invoice invoice, String routingKey) {
        String invoiceJson = null;
        try {
            objectMapper.registerModule(new JavaTimeModule());
            invoiceJson = objectMapper.writeValueAsString(invoice);
        } catch (JsonProcessingException e) {
            log.error("Error during serialization " + e);
        }
        try {
            rabbitTemplate.convertAndSend(EXCHANGE_NAME, routingKey, invoiceJson);
            log.info("Message send to exchange: {} with routing key {}", EXCHANGE_NAME, routingKey);
        }catch (Exception e){
            log.error("Exception occurred while sending message to exchange: {} with routing key {} " + e, EXCHANGE_NAME, routingKey);
        }
    }
}
