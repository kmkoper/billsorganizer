package pl.elephantcode.invoiceapi.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "invoices")
@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "description")
    private String description;

    @Column(name = "amount")
    private Float amount;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "is_recurring")
    private boolean isRecurring;

    @Column(name = "payment_period")
    private Integer paymentPeriod;

    @Column(name = "payment_period_type")
    private String paymentPeriodType;

    @Column(name = "is_current_paid")
    private boolean isCurrentPaid;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Column(name = "reminder_before_payment_value")
    private Integer reminderBeforePaymentValue;

    @Column(name = "reminder_before_payment_type")
    private String reminderBeforePaymentType;
}
