package pl.elephantcode.invoiceapi.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InvoiceConverterTest {
    final Integer ID_1 = 1;
    final String DESCRIPTION_1 = "Lorem";

    Invoice invoice;
    Invoice invoice2;

    InvoiceMapper invoiceMapper = new InvoiceMapperImpl();
    InvoiceConverter invoiceConverter;
    InvoiceDTOList invoiceDTOList;
    List<Invoice> invoiceList;

    @BeforeEach
    void setUp() {
        invoiceConverter = new InvoiceConverter(invoiceMapper);

        invoice = new Invoice();
        invoice.setId(ID_1);
        invoice.setAmount(123F);
        invoice.setUserId(2);
        invoice.setDescription(DESCRIPTION_1);

        invoice2 = new Invoice();
        invoice2.setUserId(1);
        invoice2.setDescription("Ipsum");

        invoiceDTOList = new InvoiceDTOList();

        invoiceList = new ArrayList<>();
        invoiceList.add(invoice);
        invoiceList.add(invoice2);
    }

    @Test
    void listInvoiceToInvoiceDTOListObj() {
        invoiceDTOList = invoiceConverter.listInvoiceToInvoiceDTOListObj(invoiceList);

        assertEquals(2, invoiceDTOList.getInvoiceDTOList().size());
        assertSame(invoiceDTOList.getInvoiceDTOList().get(0).getDescription(), DESCRIPTION_1);
    }
}