package pl.elephantcode.invoiceapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTO;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NextInvoiceResourceTest {

    @Mock
    NextInvoiceResource nextInvoiceResource;

    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(nextInvoiceResource).build();
    }

    @Test
    void getDataForNextInvoice() throws Exception {
        InvoiceDTO invoice1 = new InvoiceDTO();
        invoice1.setId(1);
        invoice1.setAmount(123F);

        InvoiceDTO invoice2 = new InvoiceDTO();
        invoice2.setId(2);
        invoice2.setAmount(123F);
        List<InvoiceDTO> invoiceDTOList = new ArrayList<>();
        invoiceDTOList.add(invoice1);
        invoiceDTOList.add(invoice2);

        InvoiceDTOList invoices = new InvoiceDTOList();
        invoices.setInvoiceDTOList(invoiceDTOList);

        when(nextInvoiceResource.getDataForNextInvoice()).thenReturn(invoices);
        mockMvc.perform(get(NextInvoiceResource.BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void inactivateInvoice() throws Exception {
        Invoice invoice = new Invoice();
        invoice.setAmount(123F);
        invoice.setUserId(2);

        doNothing().when(nextInvoiceResource).inactivateInvoice(anyInt());


        mockMvc.perform(post(NextInvoiceResource.BASE_URL + "/disable/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoice)))
                .andExpect(status().isOk());

        verify(nextInvoiceResource, times(1)).inactivateInvoice(anyInt());

    }
}