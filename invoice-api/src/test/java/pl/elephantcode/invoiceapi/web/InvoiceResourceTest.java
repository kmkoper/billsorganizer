package pl.elephantcode.invoiceapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.elephantcode.invoiceapi.mapper.InvoiceMapper;
import pl.elephantcode.invoiceapi.mapper.InvoiceMapperImpl;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTO;
import pl.elephantcode.invoiceapi.mapper.model.InvoiceDTOList;
import pl.elephantcode.invoiceapi.model.Invoice;
import pl.elephantcode.invoiceapi.repository.InvoiceRepository;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class InvoiceResourceTest {

    @Mock
    InvoiceResource invoiceResource;

    @Mock
    InvoiceRepository invoiceRepository;

    @InjectMocks
    InvoiceResource notMockedInvoiceResource;

    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();
    InvoiceMapper invoiceMapper = new InvoiceMapperImpl();

    Invoice invoice;
    Invoice invoice2;
    InvoiceDTOList invoicesList;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(invoiceResource).build();

        invoice = new Invoice();
        invoice.setId(1);
        invoice.setAmount(123F);
        invoice.setUserId(2);
        invoice.setDescription("Lorem");

        invoice2 = new Invoice();
        invoice2.setUserId(1);
        invoice2.setDescription("Ipsum");

        invoicesList = new InvoiceDTOList();
        invoicesList.getInvoiceDTOList().add(invoiceMapper.invoiceToInvoiceDTO(invoice));
        invoicesList.getInvoiceDTOList().add(invoiceMapper.invoiceToInvoiceDTO(invoice2));
    }

    @Test
    void findInvoice() throws Exception {
        when(invoiceResource.findInvoice(anyInt())).thenReturn(Optional.of(invoice));
        mockMvc.perform(get(InvoiceResource.BASE_URL + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void createInvoice() throws Exception {
        when(invoiceResource.createInvoice(any(InvoiceDTO.class))).thenReturn(invoice);
        mockMvc.perform(post(InvoiceResource.BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoice)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.amount", equalTo(123.0)));

        verify(invoiceResource, times(1)).createInvoice(any(InvoiceDTO.class));
    }

    @Test
    void updateInvoice() throws Exception {
        when(invoiceResource.updateInvoice(anyInt(), any(InvoiceDTO.class))).thenReturn(invoice);

        mockMvc.perform(put(InvoiceResource.BASE_URL + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoice)))
                .andExpect(status().isOk());

        verify(invoiceResource, times(1)).updateInvoice(anyInt(), any(InvoiceDTO.class));
    }

    @Test
    void updateInvoiceWhenIdNotFound() {
        Mockito.when(invoiceRepository.existsById(eq(invoice.getId()))).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> notMockedInvoiceResource.updateInvoice(invoice.getId(), any(InvoiceDTO.class)));

        verify(invoiceRepository, times(1)).existsById(anyInt());
        verify(invoiceRepository, times(0)).save(any(Invoice.class));
    }

    @Test
    void deleteInvoice() throws Exception {
        doNothing().when(invoiceResource).deleteInvoiceById(anyInt());

        mockMvc.perform(delete(InvoiceResource.BASE_URL + "/1")).andExpect(status().isOk());

        verify(invoiceResource, times(1)).deleteInvoiceById(anyInt());
    }

    @Test
    void findAllInvoicesOfUserId() throws Exception {
        when(invoiceResource.findAllInvoicesOfUserId(anyInt())).thenReturn(invoicesList);

        mockMvc.perform(get(InvoiceResource.BASE_URL + "/user/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(invoiceResource, times(1)).findAllInvoicesOfUserId(anyInt());
    }
}