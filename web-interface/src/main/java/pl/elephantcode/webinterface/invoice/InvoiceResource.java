package pl.elephantcode.webinterface.invoice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.webinterface.invoice.model.InvoiceDTO;
import pl.elephantcode.webinterface.invoice.model.InvoicesDTOList;

@Component
@Slf4j
public class InvoiceResource {

    //TODO: centralize caches!

    public static final String INVOICE_URI = "http://gatewayApi/invoice-api/api/invoices";
    @Autowired
    private RestTemplate restTemplate;

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Cacheable(value = "invoices", key = "#userId")
    public InvoicesDTOList fetchAllInvoices(Integer userId) {
        log.info("Fetching for invoices of user: {} ", userId);
        return restTemplate.getForObject(INVOICE_URI + "/user/" + userId, InvoicesDTOList.class);
    }

    @CacheEvict(value = "invoices", key = "#invoice.userId")
    public void updateInvoice(InvoiceDTO invoice) {
        log.info("Updating invoice id: {}", invoice.getId());
        restTemplate.put(INVOICE_URI + "/" + invoice.getId(), invoice, InvoiceDTO.class);
    }

    @CacheEvict(value = "invoices", key = "#invoice.userId")
    public void addNewInvoice(InvoiceDTO invoice) {
        log.info("Adding new invoice");
        restTemplate.postForObject(INVOICE_URI, invoice, InvoiceDTO.class);
    }
}
