# Bills Organizer

## Description:
Each module of this project is a single micro-app.

## How to run:
 1. Run required side apps:
    1. On docker:
        * `cd utils`
        * `docker-compose-up`
    2. Or run manually:
        * MySQL
        * MongoDB
        * RabbitMQ
        * Redis-server
 2. Set up SQL database:
    * Go to _utils_
    * Execute _bills_engine.sql_   
 3. Fork repository: [public-config-repository](https://bitbucket.org/kmkoper/config-repo-public/)
 4. Go to _config-server_ module and change in _bootstrap.yml_:
    * URI - insert url to your repository.
    * you can set **GIT_USER** and **GIT_PASS** here or as the environment variables. 
```yaml       
spring:
  cloud:
    config:
      server:
        git:
          uri:  URI
          username: ${GIT_USER}
          password: ${GIT_PASS}
```
 5. Run project:
    1. On docker:
        * Compile project - from main directory: 
            * `mvn package`
        * run config-server:
            * `cd config-server`
            * `docker-compose up -d`
        * run the rest of apps:
            * from main directory:
            * `docker-compose up -d`
    2. Or run manually each of app separately