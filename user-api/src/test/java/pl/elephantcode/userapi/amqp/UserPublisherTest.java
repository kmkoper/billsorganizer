package pl.elephantcode.userapi.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import pl.elephantcode.userapi.mapper.model.UserDTO;
import pl.elephantcode.userapi.model.User;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserPublisherTest {

    UserDTO userDTO;
    String userJSON;

    @Mock
    RabbitTemplate rabbitTemplate;

    @Mock
    ObjectMapper objectMapper;

    @InjectMocks
    UserPublisher publisher;

    ObjectMapper objectMapperWithoutMock = new ObjectMapper();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userDTO = new UserDTO();
        userDTO.setId(1);
        userDTO.setName("Test");
    }

    @Test
    void publishUserCreatedEvent() throws JsonProcessingException {
        userJSON = objectMapperWithoutMock.writeValueAsString(userDTO);
        when(objectMapper.writeValueAsString(any(User.class))).thenReturn(userJSON);

        publisher.publishUserCreatedEvent(userDTO);

        verify(rabbitTemplate, times(1)).convertAndSend(eq(null), eq(null), eq(userJSON));
    }
}