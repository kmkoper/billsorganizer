package pl.elephantcode.userapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import pl.elephantcode.userapi.model.Role;
import pl.elephantcode.userapi.repository.RoleRepository;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.elephantcode.userapi.web.RoleResource.BASE_URL;

class RoleResourceTest {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleResource roleResource;

    ObjectMapper objectMapper = new ObjectMapper();

    MockMvc mockMvc;

    Role role;

    private final Integer ROLE_ID = 1;
    private final String ROLE_ADMIN = "ADMIN";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(roleResource).build();

        role = new Role();
        role.setId(ROLE_ID);
        role.setRole(ROLE_ADMIN);

    }


    @Test
    void deleteRoleByIdWhenIdIsFound() throws Exception {
        //given
        Mockito.when(roleRepository.findById(anyInt())).thenReturn(Optional.of(role));
        Mockito.doNothing().when(roleRepository).deleteById(anyInt());

        //when
        mockMvc.perform(delete(BASE_URL + "/1"))
                .andExpect(status().isOk());

        //then
        verify(roleRepository, times(1)).deleteById(anyInt());
        verify(roleRepository, times(1)).findById(anyInt());

    }

    @Test
    void deleteRoleByIdWhenIdIsNotFoundShouldResponseWith404() throws Exception {
        //given
        Mockito.when(roleRepository.findById(anyInt())).thenReturn(Optional.ofNullable(null));
        Mockito.doNothing().when(roleRepository).deleteById(anyInt());

        //when
        mockMvc.perform(delete(BASE_URL + "/1"))
                .andExpect(status().isNotFound());

        //then
        verify(roleRepository, times(0)).deleteById(anyInt());
        verify(roleRepository, times(1)).findById(anyInt());

    }

    @Test
    void deleteRoleByIdWhenIdIsNotFoundShouldThrowException() throws Exception {
        //given
        Mockito.when(roleRepository.findById(anyInt())).thenReturn(Optional.ofNullable(null));
        Mockito.doNothing().when(roleRepository).deleteById(anyInt());

        //when
        assertThrows(ResourceNotFoundException.class,
                () -> roleResource.deleteRoleById(anyInt()));

        //then
        verify(roleRepository, times(0)).deleteById(anyInt());
        verify(roleRepository, times(1)).findById(anyInt());

    }

    @Test
    void updateRoleWhenIdIsFound() throws Exception {
        //given
        Mockito.when(roleRepository.findById(anyInt())).thenReturn(Optional.ofNullable(role));
        Mockito.when(roleRepository.save(any(Role.class))).thenReturn(role);

        //when
        mockMvc.perform(put(BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.role", equalTo(ROLE_ADMIN)));

        verify(roleRepository, times(1)).findById(anyInt());
        verify(roleRepository, times(1)).save(any(Role.class));
    }

    @Test
    void updateRoleWhenIdIsNotFoundShouldReturn404() throws Exception {
        //given
        Mockito.when(roleRepository.findById(anyInt())).thenReturn(Optional.ofNullable(null));

        //when
        mockMvc.perform(put(BASE_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isNotFound());

        verify(roleRepository, times(1)).findById(anyInt());
        verify(roleRepository, times(0)).save(any(Role.class));
    }
    @Test
    void saveRole() throws Exception {
        //given
        Mockito.when(roleRepository.findRoleByRole(ROLE_ADMIN)).thenReturn(Optional.empty());

        //when
        mockMvc.perform(post(BASE_URL)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().isCreated());

        //then
        verify(roleRepository, times(1)).findRoleByRole(anyString());
        verify(roleRepository, times(1)).save(any(Role.class));
    }

    @Test
    void saveRoleWhenRoleExistsShouldThrowException() throws Exception {
        //given
        Mockito.when(roleRepository.findRoleByRole(ROLE_ADMIN)).thenReturn(Optional.of(role));

        //when
        mockMvc.perform(post(BASE_URL)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(role)))
                .andExpect(status().is4xxClientError());

        //then
        verify(roleRepository, times(1)).findRoleByRole(anyString());
        verify(roleRepository, times(0)).save(any(Role.class));
    }
}