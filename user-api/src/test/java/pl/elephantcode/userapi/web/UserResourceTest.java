package pl.elephantcode.userapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.elephantcode.userapi.amqp.UserPublisher;
import pl.elephantcode.userapi.mapper.UserMapper;
import pl.elephantcode.userapi.mapper.model.UserDTO;
import pl.elephantcode.userapi.model.Role;
import pl.elephantcode.userapi.model.User;
import pl.elephantcode.userapi.repository.ActivationRepository;
import pl.elephantcode.userapi.repository.RoleRepository;
import pl.elephantcode.userapi.repository.UserRepository;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.elephantcode.userapi.web.UserResource.BASE_URL;
import static pl.elephantcode.userapi.web.UserResource.ROLE_USER;

class UserResourceTest {

    private static final String NAME = "Jan";

    User user;
    UserDTO userDTO;
    Role role;

    @Mock
    UserRepository userRepository;

    @Mock
    RoleRepository roleRepository;

    @Mock
    UserResource userResource;

    @Mock
    UserPublisher userPublisher;

    @Mock
    ActivationRepository activationRepository;

    @Mock
    UserMapper userMapper;

    @InjectMocks
    UserResource userResourceWithoutMock;


    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(userResource).build();

        user = new User();
        user.setName(NAME);
        user.setMail("test@test.test");
        user.setPassword("Test");
        user.setId(1);
        user.setActive(true);

        role = new Role();
        role.setRole(ROLE_USER);

        userDTO = new UserDTO();
        userDTO.setId(1);
        userDTO.setActive(true);
        userDTO.setMail("test@test.test");
        userDTO.setName("Test");
    }

    @Test
    void findUser() throws Exception {

        Mockito.when(userResource.findUser(anyInt())).thenReturn(user);
        mockMvc.perform(get(UserResource.BASE_URL + "/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(NAME)));

        verify(userResource, times(1)).findUser(anyInt());
    }

    @Test
    void findUserWhenUserIsNotPresentShouldThrowException() throws Exception {

        Mockito.when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userResourceWithoutMock.findUser(anyInt()));

        verify(userRepository, times(1)).findById(anyInt());
    }

    @Test
    void createUserWhenUserDoesNotExist() throws Exception {

        Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
        when(userRepository.findUserByMail(anyString())).thenReturn(Optional.empty());
        when(roleRepository.findRoleByRole(anyString())).thenReturn(Optional.of(role));
        when(userMapper.userToUserDTO(any(User.class))).thenReturn(userDTO);

        userResourceWithoutMock.createUser(user);

        assertEquals(1, user.getRoles().size());
        verify(userRepository, times(1)).save(any(User.class));
        verify(userRepository, times(1)).findUserByMail(anyString());
        verify(userPublisher, times(1)).publishUserCreatedEvent(any(UserDTO.class));
        assertFalse(user.isActive());
    }

    @Test
    void createUserWhenUserAlreadyExistsShouldThrowError() {

        when(userRepository.findUserByMail(anyString())).thenReturn(Optional.of(user));

        assertThrows(ResourceAlreadyExistsException.class, () -> userResourceWithoutMock.createUser(user));

    }

    @Test
    void updateUser() throws Exception {

        Mockito.when(userResource.updateUser(anyInt(), any(UserDTO.class))).thenReturn(user);
        mockMvc.perform(put(UserResource.BASE_URL + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());

        verify(userResource, times(1)).updateUser(anyInt(), any(UserDTO.class));
    }

    @Test
    void deleteUser() throws Exception {
        doNothing().when(userResource).deleteUserById(anyInt());

        mockMvc.perform(delete(UserResource.BASE_URL + "/1")).andExpect(status().isOk());

        verify(userResource, times(1)).deleteUserById(anyInt());
    }

    @Test
    void activateUserViaHTTP() throws Exception {
        when(activationRepository.getValue(anyInt())).thenReturn(user.getId());
        when(userRepository.findById(user.getId())).thenReturn(Optional.ofNullable(user));

        mockMvc.perform(put(BASE_URL + "/activate/" + user.hashCode()))
                .andExpect(status().isOk());
    }
}