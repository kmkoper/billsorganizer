package pl.elephantcode.userapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.elephantcode.userapi.model.User;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findUserByMail(String mail);
}
