package pl.elephantcode.userapi.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
public class ActivationRepository {

    @Autowired
    private RedisTemplate<Integer, Integer> redisTemplate;

    public Integer getValue(final Integer key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void setValue(final Integer key, final Integer value) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, 24, TimeUnit.HOURS);
    }
}
