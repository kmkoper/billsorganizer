package pl.elephantcode.userapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.elephantcode.userapi.model.Role;

import java.util.Optional;

public interface RoleRepository  extends JpaRepository<Role, Integer> {
    Optional<Role> findRoleByRole(String role);
}
