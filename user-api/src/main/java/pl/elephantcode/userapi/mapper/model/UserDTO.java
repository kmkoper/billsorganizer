package pl.elephantcode.userapi.mapper.model;

import lombok.Data;

@Data
public class UserDTO {
    private Integer id;
    private String name;
    private String mail;
    private boolean isActive;
}
