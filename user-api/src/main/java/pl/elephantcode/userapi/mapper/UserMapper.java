package pl.elephantcode.userapi.mapper;

import org.mapstruct.Mapper;
import pl.elephantcode.userapi.mapper.model.UserDTO;
import pl.elephantcode.userapi.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO userToUserDTO (User user);
    User userDTOToUser (UserDTO userDTO);

}
