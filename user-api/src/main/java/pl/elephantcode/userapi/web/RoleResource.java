package pl.elephantcode.userapi.web;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.userapi.model.Role;
import pl.elephantcode.userapi.repository.RoleRepository;

import java.util.List;
import java.util.Optional;

import static pl.elephantcode.userapi.web.RoleResource.BASE_URL;

@RequestMapping(BASE_URL)
@RestController
@RequiredArgsConstructor
@Slf4j
public class RoleResource {
    public static final String BASE_URL = "/api/users/roles";

    private final RoleRepository roleRepository;

    @GetMapping(value = "/{roleId}")
    public Role findRoleById(@PathVariable("roleId") Integer roleId) {
        log.info("Fetching role id: {" + roleId + "}");
        return roleRepository.findById(roleId).orElseThrow(
                () -> new ResourceNotFoundException("Role id: {" + roleId + "} not found"));
    }

    @GetMapping(value = "/find/{role}")
    public Role findRole(@PathVariable("role") String role) {
        log.info("Fething role: {" + role + "}");
        return roleRepository.findRoleByRole(role).orElseThrow(
                () -> new ResourceNotFoundException("Role: {" + role + "} not found"));
    }

    @GetMapping(value = {"", "/"})
    public List<Role> listRoles() {
        log.info("Fetching all roles");
        return roleRepository.findAll();
    }

    @PostMapping(value = {"", "/"})
    @ResponseStatus(HttpStatus.CREATED)
    public Role saveRole(@RequestBody Role role) {
        if (!(roleRepository.findRoleByRole(role.getRole()).isPresent())) {
            role.setId(null);
            roleRepository.save(role);
            log.info("Role: {" + role.getRole() + "} saved with id: {" + role.getId() + "}");
            return role;
        } else {
            throw new ResourceAlreadyExistsException("Role + {" + role.getRole() + "} already exists!");
        }
    }

    @DeleteMapping(value = "/{roleId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRoleById (@PathVariable("roleId") Integer roleId) {
        final Optional<Role> role = roleRepository.findById(roleId);
        role.orElseThrow(() -> new ResourceNotFoundException("Role id: {" + roleId + "} not found"));

        roleRepository.deleteById(role.get().getId());
        log.info("Role id: {" + roleId + "} deleted");
    }

    @PutMapping(value = {"", "/"})
    @ResponseStatus(HttpStatus.OK)
    public Role updateRole(@RequestBody Role role) {
        roleRepository.findById(role.getId())
                .orElseThrow(() ->
                new ResourceNotFoundException("Role id: {" + role.getId() + "} not found"));
        roleRepository.save(role);
        log.info("Role id: {" + role.getId() + "} updated");
        return role;
    }


}
