package pl.elephantcode.userapi.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.userapi.amqp.UserPublisher;
import pl.elephantcode.userapi.mapper.UserMapper;
import pl.elephantcode.userapi.mapper.model.UserDTO;
import pl.elephantcode.userapi.model.Role;
import pl.elephantcode.userapi.model.User;
import pl.elephantcode.userapi.repository.ActivationRepository;
import pl.elephantcode.userapi.repository.RoleRepository;
import pl.elephantcode.userapi.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static pl.elephantcode.userapi.web.UserResource.BASE_URL;

@RequestMapping(BASE_URL)
@RestController
@RequiredArgsConstructor
@Slf4j
public class UserResource {


    public static final String BASE_URL = "/api/users";
    public static final String ROLE_USER = "ROLE_USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;
    private final UserPublisher userPublisher;
    private final ActivationRepository activationRepository;

    @GetMapping(value = "/{userId}")
    public User findUser(@PathVariable("userId") Integer userId) {
        log.info("Fetching user id: " + userId);
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User id: {" + userId + "} not found"));
    }

    @GetMapping(value = "/mail/{userMail}")
    public User findUserByMail(@PathVariable("userMail") String userMail) {
        log.info("Fetching user with mail: {" + userMail + "}");
        return userRepository.findUserByMail(userMail)
                .orElseThrow(() -> new ResourceNotFoundException("User with mail: {" + userMail + "} not found"));

    }

    @GetMapping(value = {"/", ""})
    public List<User> listUsers() {
        log.info("Fetching all users");
        return userRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        if (userNotExists(user)) {
            //preparing user entity to save
            user.setActive(false); //newly registered user has always active = false
            Set<Role> userRoles = new HashSet<>();
            userRoles.add(roleRepository.findRoleByRole(ROLE_USER).orElse(new Role()));
            user.setRoles(userRoles); //newly registered user has always role = user

            userRepository.save(user); //saving user
            log.info("User {" + user.getMail() + "} saved with id: {" + user.getId() + "}");

            activationRepository.setValue(user.hashCode(), user.getId()); //adding hash and id to redis repo
            log.debug("Hash {" + user.hashCode() + "} saved to redis");

            userPublisher.publishUserCreatedEvent(userMapper.userToUserDTO(user)); //mapped to userDTO to send only necessary data
            return user;
        } else {
            throw new ResourceAlreadyExistsException("User {" + user.getMail() + "} already exists");
        }
    }

    @PutMapping(value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(
            @PathVariable("userId") Integer userId,
            @RequestBody UserDTO userDTO) {

        userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User " + userId + " not found"));

        User user = userMapper.userDTOToUser(userDTO);
        user.setId(userId);
        log.info("Saving user id: + " + userId);
        return userRepository.save(user);
    }

    @DeleteMapping(value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUserById(
            @PathVariable("userId") Integer userId) {
        final Optional<User> foundUser = userRepository.findById(userId);

        foundUser.orElseThrow(() -> new ResourceNotFoundException("User " + userId + " not found"));

        log.info("Deleting user id: " + userId);
        userRepository.deleteById(userId);
    }

    @PutMapping(value = "/activate/{activationHash}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> activateUser(@PathVariable("activationHash") Integer activationHash) {
        Integer userId = activationRepository.getValue(activationHash);
        Optional<User> user;
        if (userId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Activation link expired!");
        } else {
            user = userRepository.findById(userId);
        }
        if (user.isPresent()) {
            user.get().setActive(true);
            userRepository.save(user.get());
            log.info("User id {} activated",  user.get().getId());
            return ResponseEntity.status(HttpStatus.OK).body("User activated successfully");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User does not exists");
    }

    private boolean userNotExists(User user) {
        return !(userRepository.findUserByMail(user.getMail()).isPresent());
    }
}
