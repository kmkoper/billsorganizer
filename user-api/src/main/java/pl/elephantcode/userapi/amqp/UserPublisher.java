package pl.elephantcode.userapi.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.elephantcode.userapi.mapper.model.UserDTO;

@RequiredArgsConstructor
@Component
@Slf4j
public class UserPublisher {
    private final RabbitTemplate rabbitTemplate;
    @Value("${rabbitmq.exchange-name-topic}")
    private String EXCHANGE_NAME;
    @Value("${rabbitmq.routing-key.user-created}")
    private String USER_CREATED;
    private ObjectMapper objectMapper = new ObjectMapper();

    public void publishUserCreatedEvent(UserDTO user) {
        String userJSON = null;
        try {
            objectMapper.registerModule(new JavaTimeModule());
            userJSON = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            log.error("Error during serialization " + e);
        }
        rabbitTemplate.convertAndSend(
                EXCHANGE_NAME,
                USER_CREATED,
                userJSON);
        log.info("Message send to exchange: {} with routing key {}", EXCHANGE_NAME, USER_CREATED);
    }
}
