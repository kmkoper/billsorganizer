CREATE SCHEMA `bills_engine`;

USE `bills_engine`;

CREATE USER IF NOT EXISTS 'userAPI' IDENTIFIED BY '8GZ35RKgM2tpghcM';
CREATE USER IF NOT EXISTS 'invoiceAPI' IDENTIFIED BY 'Dz35hhHZzJpknDek';

GRANT SELECT ON `bills_engine`.* TO 'userAPI';
GRANT UPDATE ON `bills_engine`.* TO 'userAPI';
GRANT DELETE ON `bills_engine`.* TO 'userAPI';
GRANT EXECUTE ON `bills_engine`.* TO 'userAPI';
GRANT INSERT ON `bills_engine`.* TO 'userAPI';

GRANT SELECT ON `bills_engine`.* TO 'invoiceAPI';
GRANT UPDATE ON `bills_engine`.* TO 'invoiceAPI';
GRANT DELETE ON `bills_engine`.* TO 'invoiceAPI';
GRANT EXECUTE ON `bills_engine`.* TO 'invoiceAPI';
GRANT INSERT ON `bills_engine`.* TO 'invoiceAPI';

CREATE TABLE `bills_engine`.`users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) DEFAULT NULL,
                         `mail` varchar(255) DEFAULT NULL,
                         `password` varchar(255) DEFAULT NULL,
                         `is_active` tinyint(4) DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `bills_engine`.`roles` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `role` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `users_roles` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `user_id` int(11) NOT NULL,
                               `role_id` int(11) NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `id_UNIQUE` (`id`),
                               KEY `user_id_idx` (`user_id`),
                               KEY `fk_role_id_idx` (`role_id`),
                               CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
                               CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `bills_engine`.`invoices` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `description` varchar(255) DEFAULT NULL,
                            `amount` decimal(10,2) DEFAULT NULL,
                            `is_active` tinyint(4) DEFAULT NULL,
                            `user_id` int(11) DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            `is_recurring` tinyint(4) DEFAULT NULL,
                            `payment_period` int(11) DEFAULT NULL,
                            `payment_period_type` varchar(45) DEFAULT NULL,
                            `is_current_paid` tinyint(4) DEFAULT NULL,
                            `payment_date` date DEFAULT NULL,
                            `reminder_before_payment_value` int(11) DEFAULT NULL,
                            `reminder_before_payment_type` varchar(45) DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;