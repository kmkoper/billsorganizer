package pl.elephantcode.sendremindercronjob.cronjob.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.sendremindercronjob.cronjob.amqp.SendReminderPublisher;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;
import pl.elephantcode.sendremindercronjob.cronjob.web.SendReminderResource;

import java.util.HashSet;
import java.util.Set;

@Component
@Slf4j
@RequiredArgsConstructor
public class SendReminderJobService {

    private final SendReminderResource sendReminderResource;
    private final SendReminderProcessor processor;
    private final SendReminderPublisher publisher;


    public void sendReminders() {
        log.info("Send reminders invoked");

        Set<Integer> userIdSet = new HashSet<>();

        ReminderList reminders = sendReminderResource.getReminderList();

        if (!reminders.getReminders().isEmpty()) {
            reminders.getReminders().forEach(
                    reminder -> userIdSet.add(reminder.getUserId()));

            //thread for each user
            userIdSet.forEach(userId -> {
                Worker w = new Worker(userId, reminders);
                w.start();
            });
        } else {
            log.info("No reminders found to send!");
        }
    }

    @AllArgsConstructor
    private class Worker extends Thread {
        Integer userId;
        ReminderList reminders;

        @Override
        public void run() {
//            sendReminderResource.sendReminders(userId, processor.process(userId, reminders));
            ReminderList reminderList = processor.process(userId, reminders);
            publisher.publish(reminderList);
        }
    }
}
