package pl.elephantcode.sendremindercronjob.cronjob.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;

import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class SendReminderProcessor {

    ReminderList process(Integer userId, ReminderList reminders) {
        ReminderList reminderListOfGivenUser = new ReminderList();

        reminderListOfGivenUser.setReminders(reminders.getReminders().stream().filter(
                reminder -> reminder.getUserId().equals(userId)).collect(Collectors.toList()));

        log.info("Prepared list of reminders for user id {" + userId + "}");

        return reminderListOfGivenUser;
    }
}
