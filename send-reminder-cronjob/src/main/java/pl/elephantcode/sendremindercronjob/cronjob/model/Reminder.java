package pl.elephantcode.sendremindercronjob.cronjob.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Reminder {

    private String id;
    private Integer invoiceId;
    private Integer userId;
    private LocalDateTime reminderDate;
    private boolean reminderSent;
    private LocalDateTime lastReminderDate;
    private boolean lastReminderSent;
    private LocalDateTime paymentDate;
    private boolean paid;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}
