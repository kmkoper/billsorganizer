package pl.elephantcode.sendremindercronjob.cronjob;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.elephantcode.sendremindercronjob.cronjob.service.SendReminderJobService;

@Component
@Slf4j
@RequiredArgsConstructor
public class SendReminderCronScheduler {

    private final SendReminderJobService jobService;

    @Scheduled(cron = "${scheduler.sendReminder}")
    public void scheduledSendReminder(){
        jobService.sendReminders();
    }
}
