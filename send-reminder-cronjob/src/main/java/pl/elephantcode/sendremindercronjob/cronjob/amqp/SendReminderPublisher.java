package pl.elephantcode.sendremindercronjob.cronjob.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;

@Component
@Slf4j
@RequiredArgsConstructor
public class SendReminderPublisher {
    private final RabbitTemplate rabbitTemplate;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${rabbitmq.exchange-name-topic}")
    private String EXCHANGE_NAME;

    @Value("${rabbitmq.routing-key.reminder-send}")
    private String ROUTING_KEY;

    public void publish(ReminderList reminderList) {
        String reminderListJSON = null;
        try {
            objectMapper.registerModule(new JavaTimeModule());
            reminderListJSON = objectMapper.writeValueAsString(reminderList);
        } catch (JsonProcessingException e) {
            log.error("Error during serialization to json " + e);
        }
        rabbitTemplate.convertAndSend(
                EXCHANGE_NAME,
                ROUTING_KEY,
                reminderListJSON);
        log.info("Send reminder message published on queue");
    }
}
