package pl.elephantcode.sendremindercronjob.cronjob.model;

import lombok.Data;

import java.util.List;

@Data
public class ReminderList {

    private List<Reminder> reminders;
}
