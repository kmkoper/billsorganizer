package pl.elephantcode.sendremindercronjob.cronjob.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;

@Component
public class SendReminderResource {

    @LoadBalanced
    @Bean
    private RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    public ReminderList getReminderList(){
        return restTemplate.getForObject(
                "http://reminderAPI/api/reminders/send-reminder",
                ReminderList.class);
    }
}
