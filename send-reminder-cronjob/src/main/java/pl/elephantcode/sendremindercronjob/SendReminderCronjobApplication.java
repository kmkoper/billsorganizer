package pl.elephantcode.sendremindercronjob;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@EnableEurekaClient
@SpringBootApplication
@EnableScheduling
public class SendReminderCronjobApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendReminderCronjobApplication.class, args);
    }

}

