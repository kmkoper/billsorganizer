package pl.elephantcode.sendremindercronjob.cronjob.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SendReminderResourceTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    SendReminderResource resource;

    ReminderList reminderList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        reminderList = new ReminderList();
    }

    @Test
    void getReminderList() {
        when(restTemplate.getForObject(anyString(), any())).thenReturn(reminderList);
        resource.getReminderList();
        verify(restTemplate, times(1)).getForObject(anyString(), any());
    }
}