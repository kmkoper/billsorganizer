package pl.elephantcode.sendremindercronjob.cronjob.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.sendremindercronjob.cronjob.model.Reminder;
import pl.elephantcode.sendremindercronjob.cronjob.model.ReminderList;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SendReminderProcessorTest {
    public static Integer USER_ID = 1;

    SendReminderProcessor processor;

    Integer userId;
    ReminderList reminders;
    ReminderList remindersAfterProcess;
    List<Reminder> reminderList;
    Reminder reminder1;
    Reminder reminder2;

    @BeforeEach
    void setUp() {
        processor = new SendReminderProcessor();
        userId = USER_ID;
        reminders = new ReminderList();
        reminderList = new ArrayList<>();
        reminders.setReminders(reminderList);
        reminder1 = new Reminder();
        reminder2 = new Reminder();
        reminder1.setUserId(USER_ID);
        reminder2.setUserId(2);
        reminders.getReminders().add(reminder1);
        reminders.getReminders().add(reminder2);

        remindersAfterProcess = new ReminderList();
    }

    @Test
    void process() {
        remindersAfterProcess = processor.process(USER_ID, reminders);

        assertEquals(1, remindersAfterProcess.getReminders().size(),
                "Should contain 1 reminder");
        assertTrue(remindersAfterProcess.getReminders().contains(reminder1),
                "Should contain reminder with user id 1");
        assertFalse(remindersAfterProcess.getReminders().contains(reminder2),
                "Should not contain reminder with userId 2");
    }
}