package pl.elephantcode.messagemanagerapi.model.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReminderEmailMessageValidatorTest {

    ReminderEmailMessage reminderEmailMessage;
    ReminderEmailMessageValidator reminderEmailMessageValidator;
    MessageValidator messageValidator;
    List<InvoiceDTO> emptyInvoiceList;

    @BeforeEach
    void setUp() {
        messageValidator = new MessageValidator();
        reminderEmailMessage = new ReminderEmailMessage();
        reminderEmailMessageValidator = new ReminderEmailMessageValidator(messageValidator);
        emptyInvoiceList = new ArrayList<>();

        reminderEmailMessage.setReceiver("test@test.com");
        reminderEmailMessage.setName("Test name");
        reminderEmailMessage.setSubject("Test subject");
        reminderEmailMessage.setFooter("Test footer");
        reminderEmailMessage.setHeader("Test header");
        reminderEmailMessage.setSender("Test sender");
        reminderEmailMessage.setText("Test text");
        reminderEmailMessage.setInvoices(Arrays.asList(new InvoiceDTO(), new InvoiceDTO()));
    }

    @Test
    void isValid() {
        //when
        boolean valid = reminderEmailMessageValidator.isValid(reminderEmailMessage);

        //then
        assertTrue(valid);
    }

    @Test
    void isValidWhenInvoiceListIsEmpty() {
        //given
        reminderEmailMessage.setInvoices(emptyInvoiceList);

        //when
        boolean valid = reminderEmailMessageValidator.isValid(reminderEmailMessage);

        //then
        assertFalse(valid);
    }

    @Test
    void validate() {
        assertDoesNotThrow(() ->
                reminderEmailMessageValidator.validate(reminderEmailMessage),
                "Should not throw exception when message is valid");
    }

    @Test
    void validateWhenMessageInvoiceListIsEmpty() {
        reminderEmailMessage.setInvoices(emptyInvoiceList);
        assertThrows(MessageValidationException.class,
                () -> reminderEmailMessageValidator.validate(reminderEmailMessage),
                "Should throw exception when invoice list is empty");
    }
    @Test
    void validateWhenMessageIsInvalid() {
        reminderEmailMessage.setInvoices(emptyInvoiceList);
        reminderEmailMessage.setText(null);
        assertThrows(MessageValidationException.class,
                () -> reminderEmailMessageValidator.validate(reminderEmailMessage),
                "Should throw exception when message is invalid");
    }
}