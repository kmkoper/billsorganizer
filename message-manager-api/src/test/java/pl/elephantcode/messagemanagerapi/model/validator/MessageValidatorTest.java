package pl.elephantcode.messagemanagerapi.model.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.messagemanagerapi.model.message.EmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.Message;

import static org.junit.jupiter.api.Assertions.*;

class MessageValidatorTest {

    Message message;
    MessageValidator messageValidator;

    @BeforeEach
    void setUp() {
        message = new EmailMessage();
        messageValidator = new MessageValidator();

        message.setReceiver("test@test.com");
        message.setName("Test name");
        message.setSubject("Test subject");
        message.setFooter("Test footer");
        message.setHeader("Test header");
        message.setSender("Test sender");
        message.setText("Test text");
    }

    @Test
    void isValid() {
        //when
        boolean valid = messageValidator.isValid(message);

        //then
        assertTrue(valid);
    }

    @Test
    void isValidWhenMessageIsNull() {
        //given
        EmailMessage nullMessage = new EmailMessage();
        //when
        boolean valid = messageValidator.isValid(nullMessage);
        //then
        assertFalse(valid);
    }

    @Test
    void isValidWhenTextIsNull() {
        //given
        message.setText(null);
        //when
        boolean valid = messageValidator.isValid(message);
        //then
        assertFalse(valid);
    }

    @Test
    void isValidWhenTextIAndSubjectAreNulls() {
        //given
        message.setText(null);
        message.setSubject(null);
        //when
        boolean valid = messageValidator.isValid(message);
        //then
        assertFalse(valid);
    }

    @Test
    void validateWhenMessageIsNotValidShouldThrowException() {
        message.setReceiver(null);
        assertThrows(MessageValidationException.class,
                () -> messageValidator.validate(message),
                "Should throw exception when message is invalid");
    }
    @Test
    void validateWhenMessageIsValid() {
        assertDoesNotThrow(
                () -> messageValidator.validate(message),
                "Should not throw exception when message is valid");
    }
}