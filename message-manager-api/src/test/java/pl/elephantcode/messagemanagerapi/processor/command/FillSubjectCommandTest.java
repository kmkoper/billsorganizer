package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FillSubjectCommandTest {

    final String confirmationContent = "Confirmation subject";
    final String reminderContent = "Reminder subject";
    @Mock
    MessageTemplateRepository repository;
    @InjectMocks
    FillSubjectCommand fillSubjectCommand;
    ConfirmationEmailMessage confirmationEmailMessage;
    ReminderEmailMessage reminderEmailMessage;
    MessageTemplate confirmationTemplate;
    MessageTemplate reminderTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationEmailMessage = new ConfirmationEmailMessage();
        reminderEmailMessage = new ReminderEmailMessage();

        confirmationTemplate = new MessageTemplate();
        confirmationTemplate.setTemplateType(TemplateType.SUBJECT);
        confirmationTemplate.setMessageType(MessageType.CONFIRMATION);
        confirmationTemplate.setContent(confirmationContent);

        reminderTemplate = new MessageTemplate();
        reminderTemplate.setTemplateType(TemplateType.SUBJECT);
        reminderTemplate.setMessageType(MessageType.REMINDER);
        reminderTemplate.setContent(reminderContent);

    }

    @Test
    void shouldProcess() {
        fillSubjectCommand.setMessage(confirmationEmailMessage);

        assertTrue(fillSubjectCommand.shouldProcess());
    }

    @Test
    void processConfirmationEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.CONFIRMATION, TemplateType.SUBJECT))
                .thenReturn(Optional.of(confirmationTemplate));
        fillSubjectCommand.setMessage(confirmationEmailMessage);

        fillSubjectCommand.process();

        assertEquals(confirmationContent, confirmationEmailMessage.getSubject());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processReminderEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.REMINDER, TemplateType.SUBJECT))
                .thenReturn(Optional.of(reminderTemplate));
        fillSubjectCommand.setMessage(reminderEmailMessage);

        fillSubjectCommand.process();

        assertEquals(reminderContent, reminderEmailMessage.getSubject());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processWhenMessageIsNull() {
        fillSubjectCommand.process();
        verify(repository, times(0)).findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class),
                any(TemplateType.class));

    }
}