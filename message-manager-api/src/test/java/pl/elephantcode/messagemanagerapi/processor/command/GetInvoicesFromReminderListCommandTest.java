package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.reminder.Reminder;
import pl.elephantcode.messagemanagerapi.model.reminder.ReminderList;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GetInvoicesFromReminderListCommandTest {

    final String name = "name";
    final Integer id1 = 1;
    final Integer id2 = 2;

    @Mock
    PrepareMessageService messageService;
    @InjectMocks
    GetInvoicesFromReminderListCommand command;
    ReminderEmailMessage message;
    ConfirmationEmailMessage confirmationTypeMessage;
    InvoiceDTO invoice1;
    InvoiceDTO invoice2;
    Reminder reminder1;
    Reminder reminder2;
    ReminderList reminderList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationTypeMessage = new ConfirmationEmailMessage();

        message = new ReminderEmailMessage();
        message.setName(name);

        invoice1 = new InvoiceDTO();
        invoice2 = new InvoiceDTO();

        reminder1 = new Reminder();
        reminder1.setInvoiceId(id1);
        reminder2 = new Reminder();
        reminder2.setInvoiceId(id2);

        reminderList = new ReminderList();
        reminderList.setReminders(Arrays.asList(reminder1, reminder2));
        message.setReminderList(reminderList);
    }

    @Test
    void shouldProcess() {
        command.setMessage(message);
        assertTrue(command.shouldProcess());
    }


    @Test
    void shouldProcessWhenMessageDifferentInstance() {
        command.setMessage(confirmationTypeMessage);
        assertFalse(command.shouldProcess());
    }

    @Test
    void process() {
        //given
        when(messageService.getInvoiceById(id1)).thenReturn(invoice1);
        when(messageService.getInvoiceById(id2)).thenReturn(invoice2);
        command.setMessage(message);

        //when
        command.process();

        //then
        assertEquals(2, message.getInvoices().size());
        verify(messageService, times(2)).getInvoiceById(anyInt());

    }
    @Test
    void processWhenOtherInstanceIsPassed() {
        //given
        command.setMessage(confirmationTypeMessage);

        //when
        command.process();

        //then
        verify(messageService, times(0)).getInvoiceById(anyInt());

    }
}