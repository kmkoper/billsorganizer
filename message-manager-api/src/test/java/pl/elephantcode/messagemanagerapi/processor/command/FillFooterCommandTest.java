package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class FillFooterCommandTest {

    final String confirmationContent = "Confirmation footer";
    final String reminderContent = "Reminder footer";
    @Mock
    MessageTemplateRepository repository;
    @InjectMocks
    FillFooterCommand fillFooterCommand;
    ConfirmationEmailMessage confirmationEmailMessage;
    ReminderEmailMessage reminderEmailMessage;
    MessageTemplate confirmationTemplate;
    MessageTemplate reminderTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationEmailMessage = new ConfirmationEmailMessage();
        reminderEmailMessage = new ReminderEmailMessage();

        confirmationTemplate = new MessageTemplate();
        confirmationTemplate.setTemplateType(TemplateType.FOOTER);
        confirmationTemplate.setMessageType(MessageType.CONFIRMATION);
        confirmationTemplate.setContent(confirmationContent);

        reminderTemplate = new MessageTemplate();
        reminderTemplate.setTemplateType(TemplateType.FOOTER);
        reminderTemplate.setMessageType(MessageType.REMINDER);
        reminderTemplate.setContent(reminderContent);

    }

    @Test
    void shouldProcess() {
        fillFooterCommand.setMessage(confirmationEmailMessage);

        assertTrue(fillFooterCommand.shouldProcess());
    }

    @Test
    void processConfirmationEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.CONFIRMATION, TemplateType.FOOTER))
                .thenReturn(Optional.of(confirmationTemplate));
        fillFooterCommand.setMessage(confirmationEmailMessage);

        fillFooterCommand.process();

        assertEquals(confirmationContent, confirmationEmailMessage.getFooter());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processReminderEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.REMINDER, TemplateType.FOOTER))
                .thenReturn(Optional.of(reminderTemplate));
        fillFooterCommand.setMessage(reminderEmailMessage);

        fillFooterCommand.process();

        assertEquals(reminderContent, reminderEmailMessage.getFooter());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processWhenMessageIsNull() {
        fillFooterCommand.process();
        verify(repository, times(0)).findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class),
                any(TemplateType.class));

    }

}