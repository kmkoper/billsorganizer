package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static pl.elephantcode.messagemanagerapi.processor.command.FillReminderTextCommand.*;

class FillReminderTextCommandTest {

    final Integer USER_ID = 1;
    final String USER_NAME = "Test";
    final Integer INVOICE_ID = 1;
    final String HEADER_CONTENT = "header " + NAME_REPLACE;
    final String FOOTER_CONTENT = "content footer";
    final String ROW_CONTENT = "! " + NO + " " + DESCRIPTION + " " + AMOUNT + " " + DATE + " ";
    final String SUBJECT = "subject";
    final String INVOICE_DESCRIPTION = "description";
    final Float INVOICE_AMOUNT = 2.45F;
    final LocalDateTime INVOICE_DATE = LocalDateTime.now();
    @Mock
    MessageTemplateRepository templateRepository;
    @InjectMocks
    FillReminderTextCommand command;
    ReminderEmailMessage message;
    ConfirmationEmailMessage confirmationTypeMessage;
    InvoiceDTO invoiceDTO;
    MessageTemplate templateText;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        confirmationTypeMessage = new ConfirmationEmailMessage();
        message = new ReminderEmailMessage();

        invoiceDTO = new InvoiceDTO();
        templateText = new MessageTemplate();

        message.setUserId(USER_ID);
        message.setName(USER_NAME);

        invoiceDTO.setId(INVOICE_ID);
        invoiceDTO.setDescription(INVOICE_DESCRIPTION);
        invoiceDTO.setAmount(INVOICE_AMOUNT);
        invoiceDTO.setPaymentDate(INVOICE_DATE);

        message.getInvoices().add(invoiceDTO);

        message.setHeader(HEADER_CONTENT);
        message.setFooter(FOOTER_CONTENT);
        message.setSubject(SUBJECT);

        templateText.setTemplateType(TemplateType.EMAIL_TABLE_ROW);
        templateText.setMessageType(MessageType.REMINDER);
        templateText.setContent(ROW_CONTENT);

    }

    @Test
    void shouldProcess() {
        command.setMessage(message);
        assertTrue(command.shouldProcess());
    }

    @Test
    void shouldProcessWhenMessageDifferentInstance() {
        command.setMessage(confirmationTypeMessage);
        assertFalse(command.shouldProcess());
    }

    @Test
    void process() {
        when(templateRepository.findMessageTemplateByMessageTypeAndTemplateType(
                MessageType.REMINDER, TemplateType.EMAIL_TABLE_ROW)).thenReturn(Optional.of(templateText));
        command.setMessage(message);
        command.process();

        assertTrue(message.getText().contains(USER_NAME));
        assertFalse(message.getText().contains(NAME_REPLACE));

        assertTrue(message.getText().contains(INVOICE_DESCRIPTION));
        assertFalse(message.getText().contains(DESCRIPTION));

    }

}