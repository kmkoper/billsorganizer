package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class FillSenderCommandTest {

    final String confirmationContent = "Confirmation sender";
    final String reminderContent = "Reminder sender";
    @Mock
    MessageTemplateRepository repository;
    @InjectMocks
    FillSenderCommand fillSenderCommand;
    ConfirmationEmailMessage confirmationEmailMessage;
    ReminderEmailMessage reminderEmailMessage;
    MessageTemplate confirmationTemplate;
    MessageTemplate reminderTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationEmailMessage = new ConfirmationEmailMessage();
        reminderEmailMessage = new ReminderEmailMessage();

        confirmationTemplate = new MessageTemplate();
        confirmationTemplate.setTemplateType(TemplateType.SEND_FROM);
        confirmationTemplate.setMessageType(MessageType.CONFIRMATION);
        confirmationTemplate.setContent(confirmationContent);

        reminderTemplate = new MessageTemplate();
        reminderTemplate.setTemplateType(TemplateType.SEND_FROM);
        reminderTemplate.setMessageType(MessageType.REMINDER);
        reminderTemplate.setContent(reminderContent);

    }

    @Test
    void shouldProcess() {
        fillSenderCommand.setMessage(confirmationEmailMessage);

        assertTrue(fillSenderCommand.shouldProcess());
    }

    @Test
    void processConfirmationEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.CONFIRMATION, TemplateType.SEND_FROM))
                .thenReturn(Optional.of(confirmationTemplate));
        fillSenderCommand.setMessage(confirmationEmailMessage);

        fillSenderCommand.process();

        assertEquals(confirmationContent, confirmationEmailMessage.getSender());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processReminderEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.REMINDER, TemplateType.SEND_FROM))
                .thenReturn(Optional.of(reminderTemplate));
        fillSenderCommand.setMessage(reminderEmailMessage);

        fillSenderCommand.process();

        assertEquals(reminderContent, reminderEmailMessage.getSender());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processWhenMessageIsNull() {
        fillSenderCommand.process();
        verify(repository, times(0)).findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class),
                any(TemplateType.class));

    }

}