package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FillHeaderCommandTest {
    final String confirmationContent = "Confirmation header";
    final String reminderContent = "Reminder header";
    @Mock
    MessageTemplateRepository repository;
    @InjectMocks
    FillHeaderCommand fillHeaderCommand;
    ConfirmationEmailMessage confirmationEmailMessage;
    ReminderEmailMessage reminderEmailMessage;
    MessageTemplate confirmationTemplate;
    MessageTemplate reminderTemplate;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationEmailMessage = new ConfirmationEmailMessage();
        reminderEmailMessage = new ReminderEmailMessage();

        confirmationTemplate = new MessageTemplate();
        confirmationTemplate.setTemplateType(TemplateType.HEADER);
        confirmationTemplate.setMessageType(MessageType.CONFIRMATION);
        confirmationTemplate.setContent(confirmationContent);

        reminderTemplate = new MessageTemplate();
        reminderTemplate.setTemplateType(TemplateType.HEADER);
        reminderTemplate.setMessageType(MessageType.REMINDER);
        reminderTemplate.setContent(reminderContent);

    }

    @Test
    void shouldProcess() {
        fillHeaderCommand.setMessage(confirmationEmailMessage);

        assertTrue(fillHeaderCommand.shouldProcess());
    }

    @Test
    void processConfirmationEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.CONFIRMATION, TemplateType.HEADER))
                .thenReturn(Optional.of(confirmationTemplate));
        fillHeaderCommand.setMessage(confirmationEmailMessage);

        fillHeaderCommand.process();

        assertEquals(confirmationContent, confirmationEmailMessage.getHeader());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processReminderEmailMessage() {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(MessageType.REMINDER, TemplateType.HEADER))
                .thenReturn(Optional.of(reminderTemplate));
        fillHeaderCommand.setMessage(reminderEmailMessage);

        fillHeaderCommand.process();

        assertEquals(reminderContent, reminderEmailMessage.getHeader());
        verify(repository, times(1))
                .findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class), any(TemplateType.class));
    }

    @Test
    void processWhenMessageIsNull() {
        fillHeaderCommand.process();
        verify(repository, times(0)).findMessageTemplateByMessageTypeAndTemplateType(any(MessageType.class),
                any(TemplateType.class));

    }
}