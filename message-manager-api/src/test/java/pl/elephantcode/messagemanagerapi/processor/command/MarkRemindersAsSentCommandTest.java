package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.reminder.Reminder;
import pl.elephantcode.messagemanagerapi.model.reminder.ReminderList;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class MarkRemindersAsSentCommandTest {


    final String name = "name";
    final String id1 = "id1";
    final String id2 = "id2";
    @Mock
    PrepareMessageService messageService;
    @InjectMocks
    MarkRemindersAsSentCommand command;

    ReminderEmailMessage message;
    ConfirmationEmailMessage confirmationTypeMessage;
    InvoiceDTO invoice1;
    InvoiceDTO invoice2;
    Reminder reminder1;
    Reminder reminder2;
    ReminderList reminderList;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        confirmationTypeMessage = new ConfirmationEmailMessage();

        message = new ReminderEmailMessage();
        message.setName(name);

        reminder1 = new Reminder();
        reminder1.setId(id1);
        reminder1.setReminderSent(true);
        reminder2 = new Reminder();
        reminder2.setId(id2);
        reminder2.setReminderSent(true);

        reminderList = new ReminderList();
        reminderList.setReminders(Arrays.asList(reminder1, reminder2));
        message.setReminderList(reminderList);
    }

    @Test
    void shouldProcess() {
        command.setMessage(message);
        assertTrue(command.shouldProcess());
    }

    @Test
    void shouldProcessWithDifferentInstanceMessage() {
        command.setMessage(confirmationTypeMessage);
        assertFalse(command.shouldProcess());
    }

    @Test
    void process() {
        when(messageService.markReminderAsSent(anyString())).thenReturn(reminder1);
        when(messageService.markReminderAsSent(anyString())).thenReturn(reminder2);

        command.setMessage(message);
        command.process();

        verify(messageService, times(2)).markReminderAsSent(anyString());
        assertTrue(message.getReminderList().getReminders().get(0).isReminderSent());
        assertTrue(message.getReminderList().getReminders().get(1).isReminderSent());
    }
}