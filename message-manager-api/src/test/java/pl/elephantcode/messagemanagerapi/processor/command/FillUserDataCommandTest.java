package pl.elephantcode.messagemanagerapi.processor.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.messagemanagerapi.model.dto.UserDTO;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class FillUserDataCommandTest {

    final Integer userId = 1;
    final String userMail = "test@test.pl";
    final String userName = "test";

    @Mock
    PrepareMessageService messageService;

    @InjectMocks
    FillUserDataCommand command;

    ConfirmationEmailMessage confirmationEmailMessage;
    UserDTO user;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        user = new UserDTO();
        user.setId(userId);
        user.setMail(userMail);
        user.setName(userName);

        confirmationEmailMessage = new ConfirmationEmailMessage();
        confirmationEmailMessage.setUserId(userId);
    }

    @Test
    void shouldProcess() {
        command.setMessage(confirmationEmailMessage);

        assertTrue(command.shouldProcess());
    }

    @Test
    void process() {
        when(messageService.getUserById(anyInt())).thenReturn(user);

        command.setMessage(confirmationEmailMessage);
        command.process();

        assertEquals(userName, confirmationEmailMessage.getName());
        assertEquals(userMail, confirmationEmailMessage.getReceiver());

        verify(messageService, times(1)).getUserById(anyInt());
    }
    @Test
    void processWhenMessageIsNotSet() {
        command.process();

        verify(messageService, times(0)).getUserById(anyInt());
    }
}