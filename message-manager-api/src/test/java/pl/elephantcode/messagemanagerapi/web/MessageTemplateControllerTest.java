package pl.elephantcode.messagemanagerapi.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.elephantcode.messagemanagerapi.web.MessageTemplateController.BASE_URL;

class MessageTemplateControllerTest {

    @Mock
    MessageTemplateRepository repository;

    @InjectMocks
    MessageTemplateController resource;

    MessageTemplate messageTemplate;

    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(resource).build();

        messageTemplate = new MessageTemplate();
        messageTemplate.setMessageType(MessageType.CONFIRMATION);
        messageTemplate.setTemplateType(TemplateType.SUBJECT);
        messageTemplate.setContent("Please verify email");
    }

    @Test
    void addTemplate() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                any(MessageType.class), any(TemplateType.class))).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(messageTemplate)))
                .andExpect(status().isCreated());
    }

    @Test
    void addTemplateWhenTemplateAlreadyExists() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                any(MessageType.class), any(TemplateType.class))).thenReturn(Optional.ofNullable(messageTemplate));

        mockMvc.perform(post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(messageTemplate)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void getTemplate() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                MessageType.CONFIRMATION, TemplateType.SUBJECT)).thenReturn(Optional.of(messageTemplate));

        mockMvc.perform(get(BASE_URL + "/" + MessageType.CONFIRMATION + "/" + TemplateType.SUBJECT))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(objectMapper.writeValueAsString(messageTemplate)))
                .andExpect(status().isOk());
    }

    @Test
    void getTemplateWhenTemplateIsNotFoundShouldReturn404() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                MessageType.CONFIRMATION, TemplateType.SUBJECT)).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(get(BASE_URL + "/" + MessageType.CONFIRMATION + "/" + TemplateType.SUBJECT))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void removeTemplate() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                MessageType.CONFIRMATION, TemplateType.SUBJECT)).thenReturn(Optional.of(messageTemplate));

        mockMvc.perform(delete(BASE_URL + "/" + MessageType.CONFIRMATION + "/" + TemplateType.SUBJECT))
                .andExpect(status().isOk());
    }

    @Test
    void removeTemplateWhenTemplateAlreadyDoesNotExist() throws Exception {
        when(repository.findMessageTemplateByMessageTypeAndTemplateType(
                MessageType.CONFIRMATION, TemplateType.SUBJECT)).thenReturn(Optional.ofNullable(null));

        mockMvc.perform(delete(BASE_URL + "/" + MessageType.CONFIRMATION + "/" + TemplateType.SUBJECT))
                .andExpect(status().isNotFound());
    }
}