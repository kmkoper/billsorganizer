package pl.elephantcode.messagemanagerapi.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class UserCreatedConfig {

    public static final String QUEUE_NAME = "message-manager-user-created";

    @Value("${rabbitmq.routing-key.user-created}")
    private String ROUTING_KEY;

    @Bean
    Queue userCreatedQueue() {
        return new Queue(QUEUE_NAME, false);
    }


    @Bean
    Binding userCreatedBinding(Queue userCreatedQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(userCreatedQueue).to(topicExchange).with(ROUTING_KEY);
    }

}
