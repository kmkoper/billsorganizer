package pl.elephantcode.messagemanagerapi.amqp.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ExchangeConfig {

    @Value("${rabbitmq.exchange-name-topic}")
    private String EXCHANGE_NAME;

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_NAME);
    }
}
