package pl.elephantcode.messagemanagerapi.amqp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.amqp.config.SendReminderConfig;
import pl.elephantcode.messagemanagerapi.amqp.config.UserCreatedConfig;
import pl.elephantcode.messagemanagerapi.model.dto.UserDTO;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.reminder.ReminderList;
import pl.elephantcode.messagemanagerapi.processor.MessageProcessor;


@Slf4j
@Component
@RequiredArgsConstructor
public class MessageManagerListener {
    private final MessageProcessor messageProcessor;
    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues = UserCreatedConfig.QUEUE_NAME)
    public void userCreatedListener(String message) {
        log.debug("User created message received from queue");
        try {
            UserDTO user = objectMapper.readValue(message, UserDTO.class);

            ConfirmationEmailMessage confirmationEmailMessage = new ConfirmationEmailMessage();
            confirmationEmailMessage.setUserId(user.getId());
            confirmationEmailMessage.setUserHash(user.hashCode());

            log.info("Received confirmation message with hash " + user.hashCode());
            messageProcessor.process(confirmationEmailMessage);
        } catch (Exception e) {
            log.error("Exception occurs while reading message from queue: {} "  + e, UserCreatedConfig.QUEUE_NAME);
            throw new AmqpRejectAndDontRequeueException(e.getMessage());
        }
    }

    @RabbitListener(queues = SendReminderConfig.QUEUE_NAME)
    public void sendReminderListener(String message) {
        log.info("Send reminder message received from queue");
        ReminderList reminderList;
        try {
            objectMapper.registerModule(new JavaTimeModule());
            reminderList = objectMapper.readValue(message, ReminderList.class);

            ReminderEmailMessage reminderEmailMessage = new ReminderEmailMessage();
            reminderEmailMessage.setUserId(reminderList.getReminders().get(0).getUserId());
            reminderEmailMessage.setReminderList(reminderList);

            messageProcessor.process(reminderEmailMessage);
        } catch (Exception e) {
            log.error("Exception occurs while reading message from queue: {} "  + e, SendReminderConfig.QUEUE_NAME);
            throw new AmqpRejectAndDontRequeueException(e.getMessage());
        }
    }
}
