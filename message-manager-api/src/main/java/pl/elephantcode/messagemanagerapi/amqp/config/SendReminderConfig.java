package pl.elephantcode.messagemanagerapi.amqp.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SendReminderConfig {

    public static final String QUEUE_NAME = "message-manager-send-reminder";

    @Value("${rabbitmq.routing-key.reminder-send}")
    private String ROUTING_KEY;

    @Bean
    Queue sendReminderQueue() {
        return new Queue(QUEUE_NAME, false);
    }

    @Bean
    Binding sendReminderBinding(Queue sendReminderQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(sendReminderQueue).to(topicExchange).with(ROUTING_KEY);
    }
}
