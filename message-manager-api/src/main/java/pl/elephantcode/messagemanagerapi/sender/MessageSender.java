package pl.elephantcode.messagemanagerapi.sender;

public interface MessageSender<T> {

    void sendMessage(T t);
}
