package pl.elephantcode.messagemanagerapi.sender;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.CharEncoding;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pl.elephantcode.messagemanagerapi.model.message.EmailMessage;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@RequiredArgsConstructor
@Service
public class EmailMessageSender implements MessageSender<EmailMessage> {
    private final JavaMailSender javaMailSender;


    @Override
    public void sendMessage(EmailMessage emailMessage) {
        try {
            javaMailSender.send(createMimeMessage(emailMessage));
            log.info("Email message of type: {" + emailMessage.getMessageType() + "} successfully sent to: {" +
                    emailMessage.getReceiver() + "}");
        } catch (MessagingException e) {
            log.error("Cannot send email massage to: {" + emailMessage.getReceiver() + "} " + e);
        }
    }

    private MimeMessage createMimeMessage(EmailMessage emailMessage) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, CharEncoding.UTF_8);

        helper.setFrom(emailMessage.getSender());
        helper.setSubject(emailMessage.getSubject());
        helper.setTo(emailMessage.getReceiver());
        helper.setText(emailMessage.getText(), true);

        return mimeMessage;
    }
}
