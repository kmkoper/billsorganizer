package pl.elephantcode.messagemanagerapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;

import java.util.List;
import java.util.Optional;

public interface MessageTemplateRepository extends MongoRepository<MessageTemplate, String> {

    Optional<MessageTemplate> findMessageTemplateByMessageTypeAndTemplateType(MessageType messageType, TemplateType templateType);

    void deleteByMessageTypeAndTemplateType(MessageType messageType, TemplateType templateType);

    List<MessageTemplate> findAllByMessageType(MessageType messageType);
}
