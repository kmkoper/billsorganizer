package pl.elephantcode.messagemanagerapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class MessageManagerApiApplication {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(MessageManagerApiApplication.class, args);
    }

    @PostConstruct
    public void postConstruct(){

        final String hostname = env.getRequiredProperty("HOSTNAME");

        log.info("Hostname is: {" + hostname + "}");
    }


}
