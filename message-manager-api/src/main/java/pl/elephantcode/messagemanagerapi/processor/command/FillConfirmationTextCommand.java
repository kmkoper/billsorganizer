package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.ConfirmationEmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class FillConfirmationTextCommand implements Command {
    final static String HASH_REPLACE = "%hash%";
    final static String NAME_REPLACE = "%name%";

    private final MessageTemplateRepository templateRepository;

    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message instanceof ConfirmationEmailMessage;
    }

    @Override
    public void process() {
        if (shouldProcess()) {
            ConfirmationEmailMessage confirmationEmailMessage = (ConfirmationEmailMessage) message;

            String content = templateRepository.findMessageTemplateByMessageTypeAndTemplateType(
                    message.getMessageType(), TemplateType.EMAIL_TABLE_ROW).get().getContent();

            confirmationEmailMessage.setText(new StringBuilder()
                    .append(confirmationEmailMessage.getHeader().replace(NAME_REPLACE, confirmationEmailMessage.getName()))
                    .append(content.replace(HASH_REPLACE, String.valueOf(confirmationEmailMessage.getUserHash())))
                    .append(confirmationEmailMessage.getFooter())
                    .toString());

            log.debug("Text for: {" + message.getMessageType() + "} type message filled successfully");
            message = null;
        } else {
            log.warn("Fill footer command skipped");
            message = null;
        }
    }

    @Override
    public boolean isAsynchronous() {
        return false;
    }

    @Override
    public boolean isPostProcess() {
        return false;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Fill footer command cannot be run asynchronously!");
        }
    }
}
