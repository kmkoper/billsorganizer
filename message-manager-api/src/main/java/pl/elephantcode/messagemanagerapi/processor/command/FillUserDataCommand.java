package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.dto.UserDTO;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

@Slf4j
@Component
@RequiredArgsConstructor
@EqualsAndHashCode
public class FillUserDataCommand implements Command {
    private final PrepareMessageService prepareMessageService;

    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message != null;
    }

    @Override
    public void process() {
        if (shouldProcess()) {
            UserDTO user = prepareMessageService.getUserById(message.getUserId());
            message.setReceiver(user.getMail());
            message.setName(user.getName());
            log.debug("User data for user id: {" + message.getUserId() + "} filled successfully");
            message = null;
        } else {
            log.warn("Fill user data command is skipped!");
            message = null;
        }
    }

    @Override
    public boolean isAsynchronous() {
        return false;
    }

    @Override
    public boolean isPostProcess() {
        return false;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Fill user data command cannot be run asynchronously!");
        }
    }
}
