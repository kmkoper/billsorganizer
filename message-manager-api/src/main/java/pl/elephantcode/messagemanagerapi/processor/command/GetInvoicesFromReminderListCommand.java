package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.message.EmailMessage;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class GetInvoicesFromReminderListCommand implements Command{
    private final PrepareMessageService messageService;

    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message instanceof ReminderEmailMessage;
    }

    @Override
    public void process() {
        if (shouldProcess()) {
            ReminderEmailMessage reminderMessage = (ReminderEmailMessage) message;
            List<InvoiceDTO> list = new ArrayList<>();

            reminderMessage.getReminderList().getReminders().forEach(reminder ->
                list.add(messageService.getInvoiceById(reminder.getInvoiceId())));

            reminderMessage.setInvoices(list);
            log.debug("Invoices filled successfully");
            message = null;
        } else {
            log.warn("Get invoices from reminder list command is skipped!");
            message = null;
        }

    }

    @Override
    public boolean isAsynchronous() {
        return true;
    }

    @Override
    public boolean isPostProcess() {
        return false;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Get invoices command cannot be run asynchronously!");
        }
    }
}
