package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CommandContainer {
    private final FillSubjectCommand fillSubjectCommand;
    private final FillUserDataCommand fillUserDataCommand;
    private final GetInvoicesFromReminderListCommand getInvoicesFromReminderListCommand;
    private final FillFooterCommand fillFooterCommand;
    private final FillHeaderCommand fillHeaderCommand;
    private final FillSenderCommand fillSenderCommand;
    private final FillReminderTextCommand fillReminderTextCommand;
    private final MarkRemindersAsSentCommand markRemindersAsSentCommand;
    private final FillConfirmationTextCommand fillConfirmationTextCommand;

    private List<Command> allCommands;
    private List<Command> preProcessCommands;
    private List<Command> postProcessCommands;

    public List<Command> getCommands() {
        if(allCommands == null) {
            allCommands = new ArrayList<>(Arrays.asList(
                    fillUserDataCommand,
                    getInvoicesFromReminderListCommand,
                    fillSubjectCommand,
                    fillFooterCommand,
                    fillHeaderCommand,
                    fillSenderCommand,
                    fillReminderTextCommand,
                    fillConfirmationTextCommand,
                    markRemindersAsSentCommand));
        }
        return allCommands;
    }

    public List<Command> getPreProcessCommands(){
        if(preProcessCommands == null){
        preProcessCommands = new ArrayList<>();
        getCommands().forEach(command -> {
            if (!command.isPostProcess()){
                preProcessCommands.add(command);
            }
        });}
        return preProcessCommands;
    }

    public List<Command> getPostProcessCommands(){
        if(postProcessCommands == null){
            postProcessCommands = getCommands().stream().filter(Command::isPostProcess).collect(Collectors.toList());
        }
        return postProcessCommands;
    }
}
