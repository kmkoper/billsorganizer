package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

@Slf4j
@RequiredArgsConstructor
@Component
public class FillHeaderCommand implements Command {
    private final MessageTemplateRepository templateRepository;

    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message != null;
    }

    @Override
    public void process() {
        if (shouldProcess()){
            message.setHeader(templateRepository.findMessageTemplateByMessageTypeAndTemplateType(message.getMessageType(),
                    TemplateType.HEADER).get().getContent());

            log.debug("Header for: {" + message.getMessageType() + "} type message filled successfully");
            message = null;
        } else {
            log.warn("Fill header command skipped");
            message = null;
        }

    }

    @Override
    public boolean isAsynchronous() {
        return true;
    }

    @Override
    public boolean isPostProcess() {
        return false;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Fill header command cannot be run asynchronously!");
        }
    }
}
