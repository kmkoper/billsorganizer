package pl.elephantcode.messagemanagerapi.processor.command;

import pl.elephantcode.messagemanagerapi.model.message.Message;

public interface Command extends Runnable {

    void setMessage(Message message);

    boolean shouldProcess();

    void process();

    boolean isAsynchronous();

    boolean isPostProcess();

    @Override
    void run();
}
