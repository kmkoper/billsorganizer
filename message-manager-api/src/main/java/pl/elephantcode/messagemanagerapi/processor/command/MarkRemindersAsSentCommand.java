package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.web.PrepareMessageService;

@Slf4j
@Component
@RequiredArgsConstructor
public class MarkRemindersAsSentCommand implements  Command {
    private final PrepareMessageService messageService;

    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message instanceof ReminderEmailMessage;
    }

    @Override
    public void process() {
        if (shouldProcess()){
            ReminderEmailMessage reminderMessage = (ReminderEmailMessage) message;
            reminderMessage.getReminderList().getReminders().forEach(reminder -> {
                messageService.markReminderAsSent(reminder.getId());
                log.debug("Reminder id: {" + reminder.getId() + "} marked as sent successfully");
            });
            message = null;
        } else {
            log.warn("Mark reminder as sent command skipped");
            message = null;
        }

    }

    @Override
    public boolean isAsynchronous() {
        return true;
    }

    @Override
    public boolean isPostProcess() {
        return true;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Mark reminders as send command cannot be run asynchronously!");
        }

    }
}
