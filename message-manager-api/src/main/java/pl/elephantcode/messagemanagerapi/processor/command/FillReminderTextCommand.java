package pl.elephantcode.messagemanagerapi.processor.command;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class FillReminderTextCommand implements Command {
    final static String NAME_REPLACE = "%name%";
    final static String NO = "%no%";
    final static String DESCRIPTION = "%description%";
    final static String AMOUNT = "%amount%";
    final static String DATE = "%date%";
    private final MessageTemplateRepository templateRepository;
    @Setter
    private Message message;

    @Override
    public boolean shouldProcess() {
        return message instanceof ReminderEmailMessage;
    }

    @Override
    public void process() {
        if (shouldProcess()) {
            ReminderEmailMessage reminderMessage = (ReminderEmailMessage) message;

            StringBuilder invoices = new StringBuilder();
            for (int i = 0; i < reminderMessage.getInvoices().size(); i++) {
                String tableRow = templateRepository.findMessageTemplateByMessageTypeAndTemplateType(
                        reminderMessage.getMessageType(), TemplateType.EMAIL_TABLE_ROW).get().getContent();
                int no = i + 1;
                invoices.append(tableRow.replace(NO, no + ".")
                        .replace(DESCRIPTION, reminderMessage.getInvoices().get(i).getDescription())
                        .replace(AMOUNT, reminderMessage.getInvoices().get(i).getAmount().toString())
                        .replace(DATE, reminderMessage.getInvoices().get(i).getPaymentDate().toLocalDate().toString()));
            }
            reminderMessage.setText(new StringBuilder()
                    .append(reminderMessage.getHeader().replace(NAME_REPLACE, reminderMessage.getName()))
                    .append(invoices)
                    .append(reminderMessage.getFooter())
                    .toString());


            log.debug("Text for: {" + message.getMessageType() + "} type message filled successfully");
            message = null;
        } else {
            log.warn("Fill text for {" + message.getMessageType() + "} command skipped");
            message = null;
        }

    }

    @Override
    public boolean isAsynchronous() {
        return false;
    }

    @Override
    public boolean isPostProcess() {
        return false;
    }

    @Override
    public void run() {
        if (isAsynchronous()) {
            process();
        } else {
            log.error("Fill text for reminder message type command cannot be run asynchronously!");
        }
    }
}
