package pl.elephantcode.messagemanagerapi.processor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.Message;
import pl.elephantcode.messagemanagerapi.model.validator.MessageValidator;
import pl.elephantcode.messagemanagerapi.processor.command.CommandContainer;
import pl.elephantcode.messagemanagerapi.sender.MessageSender;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageProcessor {
    private final MessageValidator messageValidator;
    private final CommandContainer commandContainer;
    private final MessageSender messageSender;

    public Message process(Message message) {
        log.debug("Starting processing message type: {" + message.getMessageType() + "}");

        executePreProcessCommands(message);

        messageValidator.validate(message);

        messageSender.sendMessage(message);

        executePostProcessCommands(message);

        return message;
    }
    private void executePreProcessCommands(Message message){
        log.debug("Executing pre process commands");
        commandContainer.getPreProcessCommands()
                .forEach(command -> {
                    command.setMessage(message);
                    command.process();
                });
    }
    private void executePostProcessCommands(Message message){
        log.debug("Executing post process commands");
        commandContainer.getPostProcessCommands()
                .forEach(command -> {
                    command.setMessage(message);
                    command.process();
                });
    }
}
