package pl.elephantcode.messagemanagerapi.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.dto.UserDTO;
import pl.elephantcode.messagemanagerapi.model.reminder.Reminder;

@Service
public class PrepareMessageService {

    @Autowired
    private RestTemplate restTemplate;

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public InvoiceDTO getInvoiceById(Integer invoiceId) {
        return restTemplate.getForObject("http://invoiceAPI/api/invoices/" + invoiceId, InvoiceDTO.class);
    }

    public UserDTO getUserById(Integer userId) {
        return restTemplate.getForObject("http://userAPI/api/users/" + userId, UserDTO.class);
    }

    public Reminder markReminderAsSent(String reminderId) {
        return restTemplate.postForObject("http://reminderAPI/api/reminders/send-reminder/disable/" + reminderId, null, Reminder.class);
    }
}
