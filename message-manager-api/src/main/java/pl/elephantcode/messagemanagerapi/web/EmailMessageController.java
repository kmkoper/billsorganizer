package pl.elephantcode.messagemanagerapi.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;
import pl.elephantcode.messagemanagerapi.model.reminder.ReminderList;
import pl.elephantcode.messagemanagerapi.processor.MessageProcessor;

import static pl.elephantcode.messagemanagerapi.web.EmailMessageController.BASE_URL;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(BASE_URL)
public class EmailMessageController {
    public static final String BASE_URL = "/api/email-manager";

    private final MessageProcessor messageProcessor;

    @PostMapping(value = "/send-reminder/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void sendReminder(@PathVariable("userId") Integer userId,
                             @RequestBody ReminderList reminders) {
        log.info("Preparing email for user id: {" + userId + "}");

        ReminderEmailMessage reminderEmailMessage = new ReminderEmailMessage();
        reminderEmailMessage.setUserId(userId);
        reminderEmailMessage.setReminderList(reminders);

        messageProcessor.process(reminderEmailMessage);
    }

}
