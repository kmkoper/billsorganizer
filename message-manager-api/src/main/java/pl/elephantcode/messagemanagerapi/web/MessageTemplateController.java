package pl.elephantcode.messagemanagerapi.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.elephantcode.messagemanagerapi.model.template.MessageTemplate;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;
import pl.elephantcode.messagemanagerapi.model.template.TemplateType;
import pl.elephantcode.messagemanagerapi.repository.MessageTemplateRepository;

import java.util.List;
import java.util.Optional;

import static pl.elephantcode.messagemanagerapi.web.MessageTemplateController.BASE_URL;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(BASE_URL)
public class MessageTemplateController {
    public static final String BASE_URL = "/api/message-manager/template";

    private final MessageTemplateRepository repository;

    @PostMapping
    public ResponseEntity<MessageTemplate> addTemplate(@RequestBody MessageTemplate template) {
        if (repository.findMessageTemplateByMessageTypeAndTemplateType(
                template.getMessageType(), template.getTemplateType())
                .isPresent()) {
            log.warn("Cannot save template type: {" + template.getTemplateType()
                    + "} for message type: {" + template.getMessageType()
                    + "} Template already exists!");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } else {
            log.info("Saving template type: {" + template.getTemplateType()
                    + "} for message type: {" + template.getMessageType() + "}");
            return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(template));
        }
    }

    @GetMapping(value = "/{messageType}/{templateType}")
    public ResponseEntity<MessageTemplate> getTemplateByMessageAndTemplateTypes(
            @PathVariable("messageType") MessageType messageType,
            @PathVariable("templateType") TemplateType templateType) {
        final Optional<MessageTemplate> template = repository.findMessageTemplateByMessageTypeAndTemplateType(messageType, templateType);
        return template.map(messageTemplate -> ResponseEntity.status(HttpStatus.OK).body(messageTemplate)).orElseGet(()
                -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @GetMapping(value = {"", "/"})
    public List<MessageTemplate> getAllTemplates() {
        log.debug("Fetching all template types");
        return repository.findAll();
    }

    @DeleteMapping(value = "/{messageType}/{templateType}")
    public ResponseEntity<String> removeTemplateByMessageAndTemplateTypes(
            @PathVariable("messageType") MessageType messageType,
            @PathVariable("templateType") TemplateType templateType) {

        if (repository.findMessageTemplateByMessageTypeAndTemplateType(messageType, templateType).isPresent()) {
            repository.deleteByMessageTypeAndTemplateType(messageType, templateType);
            log.info("Removing template type: {" + templateType
                    + "} for message type: {" + messageType + "}");
            return ResponseEntity.status(HttpStatus.OK).body("Removed");
        } else {
            log.warn("Cannot remove template type: {" + templateType
                    + "} for message type: {" + messageType
                    + "} No such template found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No such template found");
        }
    }
}
