package pl.elephantcode.messagemanagerapi.model.validator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.ReminderEmailMessage;

@Slf4j
@AllArgsConstructor
@Component
public class ReminderEmailMessageValidator implements BaseValidator<ReminderEmailMessage> {
    private final MessageValidator messageValidator;

    @Override
    public boolean isValid(ReminderEmailMessage reminderEmailMessage) {
        return messageValidator.isValid(reminderEmailMessage) &&
                !(reminderEmailMessage.getInvoices().isEmpty());
    }

    @Override
    public void validate(ReminderEmailMessage reminderEmailMessage) {
        if (isValid(reminderEmailMessage)) {
            log.info("Reminder message to + {" + reminderEmailMessage.getReceiver() + "} is valid");
        } else {
            throw new MessageValidationException(
                    "Reminder message to {" + reminderEmailMessage.getReceiver() + "} is not valid");
        }
    }
}
