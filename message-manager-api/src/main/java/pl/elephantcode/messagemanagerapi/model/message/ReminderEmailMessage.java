package pl.elephantcode.messagemanagerapi.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.elephantcode.messagemanagerapi.model.dto.InvoiceDTO;
import pl.elephantcode.messagemanagerapi.model.reminder.ReminderList;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ReminderEmailMessage extends EmailMessage {

    private List<InvoiceDTO> invoices;
    private ReminderList reminderList;

    public ReminderEmailMessage() {
        invoices = new ArrayList<>();
        setMessageType(MessageType.REMINDER);
    }
}
