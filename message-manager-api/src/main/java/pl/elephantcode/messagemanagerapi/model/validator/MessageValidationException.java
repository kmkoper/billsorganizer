package pl.elephantcode.messagemanagerapi.model.validator;

public class MessageValidationException extends RuntimeException {

    public MessageValidationException(String message) {
        super(message);
    }
}
