package pl.elephantcode.messagemanagerapi.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
public class UserDTO {
    private Integer id;
    private String name;
    private String mail;
    private boolean isActive;
}
