package pl.elephantcode.messagemanagerapi.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceDTO {

    private Integer id;
    private String description;
    private Float amount;
    private boolean isActive;
    private Integer userId;
    private LocalDateTime paymentDate;
}