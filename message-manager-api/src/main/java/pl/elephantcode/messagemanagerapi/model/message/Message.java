package pl.elephantcode.messagemanagerapi.model.message;

import lombok.Data;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;

@Data
public abstract class Message {
    private String header;
    private String footer;
    private String text;
    private String sender;
    private String receiver;
    private String subject;
    private String name;
    private Integer userId;
    private MessageType messageType;
}
