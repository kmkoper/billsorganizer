package pl.elephantcode.messagemanagerapi.model.validator;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.messagemanagerapi.model.message.Message;


@Slf4j
@RequiredArgsConstructor
@Component
public class MessageValidator implements BaseValidator<Message> {

    @Override
    public boolean isValid(@NonNull Message message) {
        log.info("Validating message to {" + message.getReceiver() + "}");
        return !(message.getReceiver() == null ||
                message.getName() == null ||
                message.getSubject() == null ||
                message.getFooter() == null ||
                message.getHeader() == null ||
                message.getSender() == null ||
                message.getText() == null);
    }

    @Override
    public void validate(Message message) {
        if (isValid(message)) {
            log.info("Message to {" + message.getReceiver() + "} is valid");
        } else {
            throw new MessageValidationException(
                    "Message to {" + message.getReceiver() + "} is not valid");
        }
    }
}
