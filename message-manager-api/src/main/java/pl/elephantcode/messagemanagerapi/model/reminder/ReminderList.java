package pl.elephantcode.messagemanagerapi.model.reminder;

import lombok.Data;

import java.util.List;

@Data
public class ReminderList {

    private List<Reminder> reminders;
}
