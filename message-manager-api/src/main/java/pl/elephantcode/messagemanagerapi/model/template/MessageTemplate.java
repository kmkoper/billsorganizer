package pl.elephantcode.messagemanagerapi.model.template;

import lombok.Data;

@Data
public class MessageTemplate {
    private TemplateType templateType;
    private MessageType messageType;
    private String content;
}
