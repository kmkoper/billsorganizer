package pl.elephantcode.messagemanagerapi.model.template;

public enum MessageType {
    REMINDER,
    CONFIRMATION
}
