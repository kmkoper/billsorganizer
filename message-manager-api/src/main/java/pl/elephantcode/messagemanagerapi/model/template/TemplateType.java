package pl.elephantcode.messagemanagerapi.model.template;

public enum TemplateType {
    FOOTER,
    HEADER,
    SUBJECT,
    SEND_FROM,
    EMAIL_TABLE_ROW;
}
