package pl.elephantcode.messagemanagerapi.model.reminder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reminder {

    private String id;
    private Integer invoiceId;
    private Integer userId;
    private LocalDateTime reminderDate;
    private boolean reminderSent;
    private LocalDateTime lastReminderDate;
    private boolean lastReminderSent;
    private LocalDateTime paymentDate;
    private boolean paid;
}
