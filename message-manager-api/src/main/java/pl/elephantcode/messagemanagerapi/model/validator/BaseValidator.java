package pl.elephantcode.messagemanagerapi.model.validator;

public interface BaseValidator<T> {

    boolean isValid(T t);
    void validate(T t);
}
