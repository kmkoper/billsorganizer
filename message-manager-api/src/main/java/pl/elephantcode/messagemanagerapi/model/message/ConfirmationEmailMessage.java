package pl.elephantcode.messagemanagerapi.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.elephantcode.messagemanagerapi.model.template.MessageType;

@Data
@EqualsAndHashCode(callSuper = true)
public class ConfirmationEmailMessage extends EmailMessage {

    private int userHash;
    public ConfirmationEmailMessage() {
        setMessageType(MessageType.CONFIRMATION);
    }
}
