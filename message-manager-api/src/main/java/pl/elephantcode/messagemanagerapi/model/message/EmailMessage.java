package pl.elephantcode.messagemanagerapi.model.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EmailMessage extends Message {

}
