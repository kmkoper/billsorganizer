package pl.elephantcode.nextinvoicecronjob.cronjob.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTO;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTOList;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class NextInvoiceJobServiceTest {

    @Mock
    InvoiceResource resource;

    @Mock
    InvoiceProcessor processor;

    @InjectMocks
    NextInvoiceJobService nextInvoiceJobService;

    InvoiceDTOList invoiceDTOList;
    InvoiceDTO invoiceDTO1;
    InvoiceDTO invoiceDTO2;
    List<InvoiceDTO> list;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        invoiceDTOList = new InvoiceDTOList();
        invoiceDTO1 = new InvoiceDTO();
        invoiceDTO2 = new InvoiceDTO();
        list = new ArrayList<>();

        reset(resource);
        reset(processor);
    }


    @Test
    void createNextInvoiceWhenTwoInvoicesOnList() throws InterruptedException {
        invoiceDTO1.setActive(true);
        invoiceDTO1.setPaymentDate(LocalDateTime.now());
        invoiceDTO1.setPaymentPeriod(1);
        invoiceDTO1.setPaymentPeriodType("day");
        invoiceDTO2.setActive(true);
        invoiceDTO2.setPaymentDate(LocalDateTime.now());
        invoiceDTO2.setPaymentPeriod(1);
        invoiceDTO2.setPaymentPeriodType("day");
        list.add(invoiceDTO1);
        list.add(invoiceDTO2);
        invoiceDTOList.setInvoiceDTOList(list);

        when(resource.getInvoices()).thenReturn(invoiceDTOList);
        when(processor.process(any(InvoiceDTO.class))).thenReturn(invoiceDTO1, invoiceDTO2);
        doNothing().when(resource).saveInvoice(any(InvoiceDTO.class));
        doNothing().when(resource).inactivateInvoice(any(InvoiceDTO.class));


        nextInvoiceJobService.createNextInvoice();
        Thread.sleep(300L);


        verify(resource, times(1)).getInvoices();
        verify(resource, times(2)).inactivateInvoice(any(InvoiceDTO.class));
        verify(resource, times(2)).saveInvoice(any(InvoiceDTO.class));
    }


    @Test
    void createNextInvoiceWhenOneInvoiceOnList() throws InterruptedException {

        invoiceDTO1.setActive(true);
        invoiceDTO1.setPaymentDate(LocalDateTime.now());
        invoiceDTO1.setPaymentPeriod(1);
        invoiceDTO1.setPaymentPeriodType("day");
        list.add(invoiceDTO1);
        invoiceDTOList.setInvoiceDTOList(list);

        when(resource.getInvoices()).thenReturn(invoiceDTOList);
        when(processor.process(any(InvoiceDTO.class))).thenReturn(invoiceDTO1);
        doNothing().when(resource).saveInvoice(any(InvoiceDTO.class));
        doNothing().when(resource).inactivateInvoice(any(InvoiceDTO.class));


        nextInvoiceJobService.createNextInvoice();
        Thread.sleep(300L);


        verify(resource, times(1)).getInvoices();
        verify(resource, times(1)).inactivateInvoice(any(InvoiceDTO.class));
        verify(resource, times(1)).saveInvoice(any(InvoiceDTO.class));
    }


}