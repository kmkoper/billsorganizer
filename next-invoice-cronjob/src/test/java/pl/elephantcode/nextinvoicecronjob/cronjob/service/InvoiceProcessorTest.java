package pl.elephantcode.nextinvoicecronjob.cronjob.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTO;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


class InvoiceProcessorTest {

    InvoiceDTO invoice = new InvoiceDTO();
    InvoiceProcessor processor = new InvoiceProcessor();

    Integer interval = 2;


    @BeforeEach
    void setUp() {

        invoice.setPaymentDate(LocalDateTime.of(2019, 02, 19, 01, 00));
        invoice.setPaymentPeriod(interval);
    }

    @Test
    void processWithDay() {

        invoice.setPaymentPeriodType("day");
        processor.process(invoice);
        assertTrue(invoice.getPaymentDate().isAfter(LocalDateTime.now()));

    }

    @Test
    void processWithMonth() {

        invoice.setPaymentPeriodType("month");
        processor.process(invoice);
        assertTrue(invoice.getPaymentDate().isAfter(LocalDateTime.now()));

    }

    @Test
    void processWithYears() {

        invoice.setPaymentPeriodType("year");
        processor.process(invoice);
        assertTrue(invoice.getPaymentDate().isAfter(LocalDateTime.now()));

    }

    @Test
    void processWithIncorrectPeriodType() {
        invoice.setPaymentPeriodType("incorrect");

        assertThrows(RuntimeException.class,
                () -> processor.process(invoice));

    }
}