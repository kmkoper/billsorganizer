package pl.elephantcode.nextinvoicecronjob.cronjob.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTO;

import java.time.LocalDateTime;

@Slf4j
@AllArgsConstructor
@Component
public class InvoiceProcessor {

    InvoiceDTO process(InvoiceDTO invoice) {
        log.info("Processing invoice id: {" + invoice.getId() + "}");
        Integer interval = invoice.getPaymentPeriod();
        LocalDateTime paymentDate = invoice.getPaymentDate();
        switch (invoice.getPaymentPeriodType()) {
            case "day":
                processWithDays(invoice, interval);
                break;
            case "month":
                processWithMonths(invoice, interval);
                break;
            case "year":
                processWithYears(invoice, interval);
                break;
            default:
                log.error("Invoice id: {" + invoice.getId() + "} payment period type {" + invoice.getPaymentPeriodType() + "} not recognised!");
                throw new RuntimeException("Payment period type not recognised!");
        }
        return invoice;
    }

    private InvoiceDTO processWithDays(InvoiceDTO invoice, Integer interval) {

        while (invoice.getPaymentDate().isBefore(LocalDateTime.now())) {
            LocalDateTime paymentDate = invoice.getPaymentDate();
            invoice.setPaymentDate(paymentDate.plusDays(interval));
        }
        return invoice;
    }

    private InvoiceDTO processWithMonths(InvoiceDTO invoice, Integer interval) {

        while (invoice.getPaymentDate().isBefore(LocalDateTime.now())) {
            LocalDateTime paymentDate = invoice.getPaymentDate();
            invoice.setPaymentDate(paymentDate.plusMonths(interval));
        }
        return invoice;
    }
    private InvoiceDTO processWithYears(InvoiceDTO invoice, Integer interval) {

        while (invoice.getPaymentDate().isBefore(LocalDateTime.now())) {
            LocalDateTime paymentDate = invoice.getPaymentDate();
            invoice.setPaymentDate(paymentDate.plusYears(interval));
        }
        return invoice;
    }
}
