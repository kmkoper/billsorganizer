package pl.elephantcode.nextinvoicecronjob.cronjob;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.elephantcode.nextinvoicecronjob.cronjob.service.NextInvoiceJobService;

@Component
@Slf4j
@RequiredArgsConstructor
public class NextInvoiceCronScheduler {

    private final NextInvoiceJobService nextInvoiceJobService;

    @Scheduled(cron = "${scheduler.createNextInvoice}")
    public void scheduledCreateNextInvoice() {
        nextInvoiceJobService.createNextInvoice();
    }
}
