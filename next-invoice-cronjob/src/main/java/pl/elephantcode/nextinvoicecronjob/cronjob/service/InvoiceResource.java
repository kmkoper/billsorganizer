package pl.elephantcode.nextinvoicecronjob.cronjob.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTO;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTOList;

@Component
public class InvoiceResource {


    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    InvoiceDTOList getInvoices() {
        return restTemplate.getForObject(
                "http://invoiceAPI/api/invoices/create-next",
                InvoiceDTOList.class);
    }

    void saveInvoice(InvoiceDTO invoiceDTO) {
        restTemplate.postForObject(
                "http://invoiceAPI/api/invoices",
                invoiceDTO,
                InvoiceDTO.class);
    }

    void inactivateInvoice(InvoiceDTO invoice){
        restTemplate.postForObject(
                "http://invoiceAPI/api/invoices/create-next/disable/" + invoice.getId(),
                invoice,
                InvoiceDTO.class);
    }
}
