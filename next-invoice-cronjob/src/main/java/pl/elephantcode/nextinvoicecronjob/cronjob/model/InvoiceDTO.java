package pl.elephantcode.nextinvoicecronjob.cronjob.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class InvoiceDTO {

    private Integer id;
    private String description;
    private Float amount;
    private boolean isActive;
    private boolean isRecurring;
    private Integer paymentPeriod;
    private String paymentPeriodType;
    private Integer userId;
    private LocalDateTime paymentDate;
    private Integer reminderBeforePaymentValue;
    private String reminderBeforePaymentType;
}