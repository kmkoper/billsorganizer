package pl.elephantcode.nextinvoicecronjob.cronjob.model;

import lombok.Data;

import java.util.List;

@Data
public class InvoiceDTOList {
    private List<InvoiceDTO> invoiceDTOList;
}
