package pl.elephantcode.nextinvoicecronjob.cronjob.service;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTO;
import pl.elephantcode.nextinvoicecronjob.cronjob.model.InvoiceDTOList;

@Component
@Slf4j
@RequiredArgsConstructor
public class NextInvoiceJobService {

    private final InvoiceResource resource;
    private final InvoiceProcessor processor;


    public void createNextInvoice() {
        log.info("Create Next invoice invoked.");


        InvoiceDTOList invoices = resource.getInvoices();

        if (!invoices.getInvoiceDTOList().isEmpty()) {
            invoices.getInvoiceDTOList().forEach(
                    invoice -> {
                        Worker w = new Worker(invoice);
                        w.start();
                    });
        } else {
            log.info("No candidates found to create next invoice!!");
        }
    }

    @AllArgsConstructor
    private class Worker extends Thread {

        private InvoiceDTO invoice;

        @Override
        public void run() {
            resource.inactivateInvoice(invoice);
            resource.saveInvoice(processor.process(invoice));
        }
    }
}

