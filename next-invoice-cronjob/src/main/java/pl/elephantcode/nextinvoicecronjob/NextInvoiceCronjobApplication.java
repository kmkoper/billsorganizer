package pl.elephantcode.nextinvoicecronjob;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@EnableEurekaClient
@SpringBootApplication
@EnableScheduling
public class NextInvoiceCronjobApplication {

    public static void main(String[] args) {
        SpringApplication.run(NextInvoiceCronjobApplication.class, args);
    }

}

